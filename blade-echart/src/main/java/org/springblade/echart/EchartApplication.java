package org.springblade.echart;

import org.springblade.core.launch.BladeApplication;
import org.springframework.cloud.client.SpringCloudApplication;

@SpringCloudApplication
public class EchartApplication {
	
	public static void main(String[] args) {
		BladeApplication.run("blade-echart", EchartApplication.class, args);
	}

}
