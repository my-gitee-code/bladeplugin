package org.springblade.echart.config;

import org.springblade.core.secure.registry.SecureRegistry;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MvcConfig implements WebMvcConfigurer {
	
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/");
	}
	
//	@Bean
//	public SecureRegistry secureRegistry() {
//		SecureRegistry secureRegistry = new SecureRegistry();
////		secureRegistry.setEnabled(true);
//		secureRegistry.excludePathPattern("/static/**");
//		return secureRegistry;
//	}

}
