package org.springblade.echart.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springblade.core.tool.api.R;
import org.springblade.echart.entity.DataSet;
import org.springblade.echart.entity.DataSource;
import org.springblade.echart.mapper.DataSetMapper;
import org.springblade.echart.service.IDataSetService;
import org.springblade.echart.service.IDataSourceService;
import org.springblade.echart.utils.DbUtils;
import org.springblade.echart.vo.DataSetParamItem;
import org.springblade.echart.vo.DataSetVO;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("data/set")
@Api(tags = "数据集")
public class DataSetController {

	@Resource
	private IDataSourceService sourceService;
	@Resource
	private IDataSetService dataSetService;
	@Resource
	private DataSetMapper dataSetMapper;

	@ResponseBody
	@PostMapping("save")
	@ApiOperation(value = "新增")
	public R insert(@RequestBody DataSetVO entity) {
		if (entity.getStatus() == null) {
			entity.setStatus(0);
		}
		entity.setCreateTime(new Date());
		if (entity.getId() == null || entity.getId() == 0) {
			entity.setId(null);
			dataSetService.save(entity.toDataSet());
		} else {
			dataSetService.updateById(entity.toDataSet());
		}
		return R.success("success");
	}
	
	@ResponseBody
	@DeleteMapping("{id}")
	@ApiOperation(value = "删除")
	public R delete(@PathVariable("id") Integer id) {
		dataSetService.removeById(id);
		return R.success("success");
	}

	@ResponseBody
	@GetMapping("{id}")
	@ApiOperation(value = "详情")
	public R details(@PathVariable("id") Integer id) {
		DataSet dataSet = dataSetService.getById(id);
		if (dataSet != null) {
			return R.data(dataSet.toDataSetVO());
		} else {
			return R.fail("数据不存在");
		}
	}

	@ResponseBody
	@GetMapping("query")
	@ApiOperation(value = "分页查询")
	public R query(@RequestParam(required = false) Integer page, @RequestParam(required = false) Integer size) {
		if (page == null || page == 0) {
			page = 1;
		}
		if (size == null || size == 0) {
			size = 20;
		}
		
		IPage<DataSetVO> pageDatIPage = new Page<>();
		pageDatIPage.setCurrent(page);
		pageDatIPage.setSize(size);
		pageDatIPage.setTotal(dataSetService.count());
		
		List<DataSet> dataSets = dataSetMapper.query((page - 1) * size, size);
		
		List<DataSetVO> dataSetVOs = new ArrayList<>();
		dataSets.forEach(Item -> {
			dataSetVOs.add(Item.toDataSetVO());
		});
		pageDatIPage.setRecords(dataSetVOs);
		
		return R.data(pageDatIPage);
	}

	@ResponseBody
	@GetMapping("data")
	@ApiOperation(value = "获取统计数据")
	public R getReportData(HttpServletRequest request, @RequestParam Integer id) {
		//这里不要使用hashmap，因为它会自动进行排序
		Map<String, String> params = new LinkedHashMap<>();
	    Enumeration enumeration = request.getParameterNames();
	    while (enumeration.hasMoreElements()) {
	        String name = String.valueOf(enumeration.nextElement());
	        if (!name.equals("id")) {
	        	params.put(name, request.getParameter(name));
	        }
	    }
	    
		DataSet dataSet = dataSetService.getById(id);
		if (dataSet == null) {
			return R.fail("数据集不存在");
		}
		if (dataSet.getQuery() == null || dataSet.getQuery().isEmpty()) {
			return R.fail("该数据集的查询SQL为空");
		}
		
		DataSource dataSource = sourceService.getById(dataSet.getSourceId());
		if (dataSource == null) {
			return R.fail("数据源不存在");
		}

		String sql = dataSet.getQuery();
		List<Object> paramList = new ArrayList<>();
		// 默认值参数及值
		List<DataSetParamItem> defaultParams = dataSet.toDataSetVO().getParams();
		for (DataSetParamItem item : defaultParams) {
			String str = String.format("{%s}", item.getName());
			if (!params.containsKey(item.getName())) {
				params.put(item.getName(), item.getDefaultValue());
			}
			if (sql.indexOf(str) != -1) {
				sql = sql.replace(str, "?");
				paramList.add(params.get(item.getName()));
			}
		}
		
		// 原始数据
		List<Map<String, Object>> echartData = DbUtils.getJdbcTemplate(dataSource).queryForList(sql, paramList.toArray());
		
		return R.data(echartData);
	}

	@GetMapping("edit")
	@ApiOperation(value = "编辑")
	public String edit(Model model, @RequestParam(defaultValue = "0") Integer id) {
		model.addAttribute("title", "数据集");
		model.addAttribute("id", id);
		return "data/set/edit";
	}

	@GetMapping("index")
	@ApiOperation(value = "首页")
	public String index(Model model) {
		model.addAttribute("title", "数据集");
		return "data/set/index";
	}
}
