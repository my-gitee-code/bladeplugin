package org.springblade.echart.controller;

import java.util.Date;

import javax.annotation.Resource;

import org.springblade.core.tool.api.R;
import org.springblade.echart.entity.DataSource;
import org.springblade.echart.service.IDataSourceService;
import org.springblade.echart.utils.DbUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("data/source")
@Api(tags = "数据源")
public class DataSourceController {
	
	@Resource
	private IDataSourceService sourceService;
	
	@ResponseBody
	@PostMapping("save")
	@ApiOperation(value = "新增或修改")
	public R insert(@RequestBody DataSource entity) {
		if (entity.getStatus() == null) {
			entity.setStatus(0);
		}
		entity.setCreateTime(new Date());
		if (entity.getId() == null || entity.getId() == 0) {
			entity.setId(null);
			sourceService.save(entity);
		} else {
			sourceService.updateById(entity);
		}
		return R.data(entity);
	}
	
	@ResponseBody
	@DeleteMapping("{id}")
	@ApiOperation(value = "删除")
	public R delete(@PathVariable("id") Integer id) {
		sourceService.removeById(id);
		return R.success("success");
	}

	@ResponseBody
	@GetMapping("{id}")
	@ApiOperation(value = "详情")
	public R details(@PathVariable("id") Integer id) {
		return R.data(sourceService.getById(id));
	}

	@ResponseBody
	@GetMapping("query")
	@ApiOperation(value = "分页查询")
	public R query(@RequestParam(required = false) Integer page, @RequestParam(required = false) Integer size) {
		if (page == null || page == 0) {
			page = 1;
		}
		if (size == null || size == 0) {
			size = 20;
		}
		
		LambdaQueryWrapper<DataSource> lqw = new LambdaQueryWrapper<>();
		IPage<DataSource> pageDataSource = new Page<>(page, size);
		pageDataSource = sourceService.page(pageDataSource, lqw);
		return R.data(pageDataSource);
	}

	@ResponseBody
	@GetMapping("tables")
	@ApiOperation(value = "所有数据表")
	public R tables(@RequestParam Integer id) {
		DataSource dataSource = sourceService.getById(id);
		if (dataSource == null) {
			return R.fail("记录不存在");
		}
		
		JdbcTemplate jdbcTemplate = DbUtils.getJdbcTemplate(dataSource);
		return R.data(DbUtils.getTables(jdbcTemplate));
	}
	
	@ResponseBody
	@GetMapping("fields")
	@ApiOperation(value = "所有字段")
	public R fields(@RequestParam Integer id, @RequestParam String table) {
		DataSource dataSource = sourceService.getById(id);
		if (dataSource == null) {
			return R.fail("记录不存在");
		}
		
		JdbcTemplate jdbcTemplate = DbUtils.getJdbcTemplate(dataSource);
		return R.data(DbUtils.getFields(jdbcTemplate, table));
	}

	@GetMapping("edit")
	@ApiOperation(value = "编辑")
	public String edit(Model model, @RequestParam(defaultValue = "0") Integer id) {
		model.addAttribute("title", "数据源");
		model.addAttribute("id", id);
		return "data/source/edit";
	}

	@GetMapping("index")
	@ApiOperation(value = "首页")
	public String index(Model model) {
		model.addAttribute("title", "数据源");
		return "data/source/index";
	}

}
