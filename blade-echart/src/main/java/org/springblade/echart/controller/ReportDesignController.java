package org.springblade.echart.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springblade.core.tool.api.R;
import org.springblade.echart.entity.DataSet;
import org.springblade.echart.entity.DataSource;
import org.springblade.echart.entity.ReportModel;
import org.springblade.echart.service.IDataSetService;
import org.springblade.echart.service.IDataSourceService;
import org.springblade.echart.service.IReportModelService;
import org.springblade.echart.utils.DbUtils;
import org.springblade.echart.vo.DataSetParamItem;
import org.springblade.echart.vo.EchartDataVO;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("report/design")
@Api(tags = "报表设计")
public class ReportDesignController {
	
	@Resource
	private IDataSourceService dataSourceService;
	@Resource
	private IDataSetService dataSetService;
	@Resource
	private IReportModelService reportModelService;
	
	@ResponseBody
	@PostMapping("save")
	@ApiOperation(value = "新增")
	public R insert(@RequestBody EchartDataVO entity) {
		if (entity.getStatus() == null) {
			entity.setStatus(0);
		}
		entity.setCreateTime(new Date());
		
		ReportModel reportModel = new ReportModel();
		BeanUtils.copyProperties(entity, reportModel);
		reportModel.setDataFields(entity.getDataFields().toJSONString());
		
		if (entity.getId() == null || entity.getId() == 0) {
			reportModelService.save(reportModel);
		} else {
			reportModelService.updateById(reportModel);
		}
		
		return R.data(reportModel);
	}
	
	@ResponseBody
	@DeleteMapping("{id}")
	@ApiOperation(value = "删除")
	public R delete(@PathVariable("id") Integer id) {
		reportModelService.removeById(id);
		return R.success("success");
	}

	@ResponseBody
	@GetMapping("{id}")
	@ApiOperation(value = "详情")
	public R details(@PathVariable("id") Integer id) {
		ReportModel reportModel = reportModelService.getById(id);
		if (reportModel == null) {
			return R.fail("数据不存在");
		}
		
		EchartDataVO entity = new EchartDataVO();
		BeanUtils.copyProperties(reportModel, entity);
		entity.setDataFields(JSONArray.parseArray(reportModel.getDataFields()));
		
		return R.data(entity);
	}

	@ResponseBody
	@GetMapping("query")
	@ApiOperation(value = "分页查询")
	public R query(@RequestParam(required = false) Integer page, @RequestParam(required = false) Integer size) {
		if (page == null || page == 0) {
			page = 1;
		}
		if (size == null || size == 0) {
			size = 20;
		}
		
		LambdaQueryWrapper<ReportModel> lqw = new LambdaQueryWrapper<>();
		IPage<ReportModel> pageDataSource = new Page<>(page, size);
		pageDataSource = reportModelService.page(pageDataSource, lqw);
		return R.data(pageDataSource);
	}
	
	@ResponseBody
	@GetMapping("data")
	@ApiOperation(value = "获取统计数据")
	public R getReportData(HttpServletRequest request, @RequestParam Integer id) {
		//这里不要使用hashmap，因为它会自动进行排序
		Map<String, String> params = new LinkedHashMap<>();
	    Enumeration enumeration = request.getParameterNames();
	    while (enumeration.hasMoreElements()) {
	        String name = String.valueOf(enumeration.nextElement());
	        if (!name.equals("id")) {
	        	params.put(name, request.getParameter(name));
	        }
	    }
	    
		// 报表模型
		ReportModel reportModel = reportModelService.getById(id);
		if (reportModel == null) {
			return R.fail("数据不存在");
		}
				
		DataSet dataSet = dataSetService.getById(reportModel.getDatasetId());
		if (dataSet == null) {
			return R.fail("数据集不存在");
		}
		if (dataSet.getQuery() == null || dataSet.getQuery().isEmpty()) {
			return R.fail("该数据集的查询SQL为空");
		}
		
		DataSource dataSource = dataSourceService.getById(dataSet.getSourceId());
		if (dataSource == null) {
			return R.fail("数据源不存在");
		}

		String sql = dataSet.getQuery();
		List<Object> paramList = new ArrayList<>();
		// 默认值参数及值
		List<DataSetParamItem> defaultParams = dataSet.toDataSetVO().getParams();
		for (DataSetParamItem item : defaultParams) {
			String str = String.format("{%s}", item.getName());
			if (!params.containsKey(item.getName())) {
				params.put(item.getName(), item.getDefaultValue());
			}
			if (sql.indexOf(str) != -1) {
				sql = sql.replace(str, "?");
				paramList.add(params.get(item.getName()));
			}
		}
		
		// 原始数据
		List<Map<String, Object>> echartData = DbUtils.getJdbcTemplate(dataSource).queryForList(sql, paramList.toArray());
		
		EchartDataVO entity = new EchartDataVO();
		BeanUtils.copyProperties(reportModel, entity);
		entity.setDataFields(JSONArray.parseArray(reportModel.getDataFields()));
		
		List<String> legends = new ArrayList<>();
		List<String> xaxis = new ArrayList<>();
		List<Object> seriesData = new ArrayList<>();
		
		// 找出横纵字段
		String xName = "", yName = "", echartType = "";
		for (Integer i=0; i<entity.getDataFields().size(); i++) {
			JSONObject item = entity.getDataFields().getJSONObject(i);
			String name = item.getString("name");
			String type = item.getString("type");
			if (type.equals("xAxis")) {
				xName = name;
			} else if (!type.isEmpty()) {
				yName = name;
				echartType = type;
			}
		}
		
		// 找出横纵字段数据
		for (Map<String, Object> item : echartData) {
			if (!xName.isEmpty() && item.containsKey(xName) && !xaxis.contains(item.get(xName).toString())) {
				xaxis.add(item.get(xName).toString());
			}
			if (!yName.isEmpty() && item.containsKey(yName) && !legends.contains(item.get(yName).toString())) {
				legends.add(item.get(yName).toString());
			}
		}
		
		if (echartType.equals("pie") || echartType.equals("ring")) {
			for (Map<String, Object> item : echartData) {
				JSONObject json = new JSONObject();
				json.put("name", item.get(yName).toString());
				json.put("value", item.get(entity.getCensusField()));
				seriesData.add(json);
			}
		} else if (xaxis.size() == echartData.size()){
			List<Object> data = new ArrayList<>();
			for (Map<String, Object> item : echartData) {
				data.add(item.get(entity.getCensusField()));
			}
			seriesData.add(data);
		} else {
			for (Integer i=0; i<legends.size(); i++) {
				List<Object> data = new ArrayList<>();
				for (Integer j=0; j<xaxis.size(); j++) {
					boolean find = false;
					for (Map<String, Object> item : echartData) {
						log.info("{}", item);
						if (item.get(xName).toString().equals(xaxis.get(j)) && item.get(yName).toString().equals(legends.get(i))) {
							find = true;
							data.add(item.get(entity.getCensusField()));
							break;
						}
					}
					if (!find) {
						data.add(0);
					}
				}
				seriesData.add(data);
			}
		}
		
		entity.setType(echartType);
		entity.setLegends(legends);
		entity.setXaxis(xaxis);
		entity.setSeries(seriesData);
		
		return R.data(entity);
	}

	@GetMapping("edit")
	@ApiOperation(value = "编辑")
	public String edit(Model model, @RequestParam(defaultValue = "0") Integer id) {
		model.addAttribute("title", "报表设计");
		model.addAttribute("id", id);
		return "report/design/edit";
	}

	@GetMapping("preview")
	@ApiOperation(value = "预览")
	public String preview(Model model, @RequestParam Integer id) {
		model.addAttribute("title", "报表预览");
		model.addAttribute("id", id);
		return "report/design/preview";
	}

	@GetMapping("index")
	@ApiOperation(value = "首页")
	public String index(Model model) {
		model.addAttribute("title", "报表设计");
		return "report/design/index";
	}
	
}
