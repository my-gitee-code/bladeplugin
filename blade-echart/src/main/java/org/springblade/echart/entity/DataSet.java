package org.springblade.echart.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springblade.echart.vo.DataSetDescribeItem;
import org.springblade.echart.vo.DataSetParamItem;
import org.springblade.echart.vo.DataSetVO;
import org.springframework.beans.BeanUtils;
import org.springframework.data.annotation.Id;
import org.springframework.format.annotation.DateTimeFormat;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@TableName("tb_report_dataset")
@ApiModel(value="DataSet", description="数据集")
public class DataSet {

	@Id
	@TableId(value = "id", type = IdType.AUTO)
	private Integer id;
	
	@ApiModelProperty(value = "名称")
	private String name;

	@ApiModelProperty(value = "所属数据源ID")
	private Integer sourceId;
	
	@TableField(exist = false)
	@ApiModelProperty(value = "所属数据源名称")
	private String sourceName;

	@ApiModelProperty(value = "表名")
	private String tableName;

	@ApiModelProperty(value = "查询SQL")
	private String query;

	@ApiModelProperty(value = "描述")
	private String description;

	@ApiModelProperty(value = "配置，json数据")
	private String deploy;

	@ApiModelProperty(value = "查询参数，json数据")
	private String param;
	
	@ApiModelProperty(value = "状态：1=启用")
	private Integer status;

	@ApiModelProperty(value = "新增或修改时间")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
	private Date createTime;
	
	/**
	 * 转换为前端对象
	 * @return
	 */
	public DataSetVO toDataSetVO() {
		DataSetVO dataSetVO = new DataSetVO();
		BeanUtils.copyProperties(this, dataSetVO);
		
		List<DataSetDescribeItem> deploys = new ArrayList<>();
		
		if (getDeploy() != null && !getDeploy().isEmpty()) {
//			deploys = JSON.parseObject(getDeploy(), deploys.getClass());
			
			JSONArray jsonArray = JSON.parseArray(getDeploy());
			for (Integer i=0; i < jsonArray.size(); i++) {
				JSONObject item = jsonArray.getJSONObject(i);
				DataSetDescribeItem describeItem = JSON.parseObject(JSON.toJSONString(item), DataSetDescribeItem.class);
				deploys.add(describeItem);
			}
		}
		
		List<DataSetParamItem> params = new ArrayList<>();
		if (getParam() != null && !getParam().isEmpty()) {
//			params = JSON.parseObject(getParam(), params.getClass());
			
			JSONArray jsonArray = JSON.parseArray(getParam());
			for (Integer i=0; i < jsonArray.size(); i++) {
				JSONObject item = jsonArray.getJSONObject(i);
				DataSetParamItem describeItem = JSON.parseObject(JSON.toJSONString(item), DataSetParamItem.class);
				params.add(describeItem);
			}
		}
		
		dataSetVO.setDeploys(deploys);
		dataSetVO.setParams(params);
		
		return dataSetVO;
	}

}
