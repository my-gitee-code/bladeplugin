package org.springblade.echart.entity;

import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.format.annotation.DateTimeFormat;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@TableName("tb_report_model")
@ApiModel(value="ReportModel", description="图表模型")
public class ReportModel {

	@Id
	@TableId(value = "id", type = IdType.AUTO)
	private Integer id;
	
	@ApiModelProperty(value = "名称")
	private String name;

	@ApiModelProperty(value = "数据集ID")
	private Integer datasetId;

	@ApiModelProperty(value = "数据字段,json数据")
	private String dataFields;

	@ApiModelProperty(value = "统计字段")
	private String censusField;

	@ApiModelProperty(value = "单位")
	private String unit;
	
	@ApiModelProperty(value = "描述")
	private String description;
	
	@ApiModelProperty(value = "状态：1=启用")
	private Integer status;

	@ApiModelProperty(value = "新增或修改时间")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
	private Date createTime;

}
