package org.springblade.echart.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Select;
import org.springblade.echart.entity.DataSet;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface DataSetMapper extends BaseMapper<DataSet> {
	
	/**
	 * 分页查询
	 */
	@Select("select a.*,b.name as sourceName from tb_report_dataset as a left join tb_report_datasource as b on a.source_id=b.id order by a.create_time desc limit #{index}, #{size}")
	List<DataSet> query(Integer index, Integer size);

}
