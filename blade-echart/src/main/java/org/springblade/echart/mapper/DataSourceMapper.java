package org.springblade.echart.mapper;

import org.springblade.echart.entity.DataSource;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface DataSourceMapper extends BaseMapper<DataSource> {

}
