package org.springblade.echart.mapper;

import org.springblade.echart.entity.ReportModel;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface ReportModelMapper extends BaseMapper<ReportModel> {

}
