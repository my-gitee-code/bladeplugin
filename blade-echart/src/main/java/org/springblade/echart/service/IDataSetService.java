package org.springblade.echart.service;

import org.springblade.echart.entity.DataSet;

import com.baomidou.mybatisplus.extension.service.IService;

public interface IDataSetService extends IService<DataSet> {

}
