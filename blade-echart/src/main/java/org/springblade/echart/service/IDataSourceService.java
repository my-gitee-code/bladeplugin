package org.springblade.echart.service;

import org.springblade.echart.entity.DataSource;

import com.baomidou.mybatisplus.extension.service.IService;

public interface IDataSourceService extends IService<DataSource> {

}
