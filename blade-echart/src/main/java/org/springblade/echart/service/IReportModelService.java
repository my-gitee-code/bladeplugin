package org.springblade.echart.service;

import org.springblade.echart.entity.ReportModel;

import com.baomidou.mybatisplus.extension.service.IService;

public interface IReportModelService extends IService<ReportModel> {

}
