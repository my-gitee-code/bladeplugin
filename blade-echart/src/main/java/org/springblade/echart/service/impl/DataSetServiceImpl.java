package org.springblade.echart.service.impl;

import org.springblade.echart.entity.DataSet;
import org.springblade.echart.mapper.DataSetMapper;
import org.springblade.echart.service.IDataSetService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

@Service
public class DataSetServiceImpl extends ServiceImpl<DataSetMapper, DataSet> implements IDataSetService {

}
