package org.springblade.echart.service.impl;

import org.springblade.echart.entity.DataSource;
import org.springblade.echart.mapper.DataSourceMapper;
import org.springblade.echart.service.IDataSourceService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

@Service
public class DataSourceServiceImpl extends ServiceImpl<DataSourceMapper, DataSource> implements IDataSourceService {

}
