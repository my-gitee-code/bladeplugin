package org.springblade.echart.service.impl;

import org.springblade.echart.entity.ReportModel;
import org.springblade.echart.mapper.ReportModelMapper;
import org.springblade.echart.service.IReportModelService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

@Service
public class ReportModelServiceImpl extends ServiceImpl<ReportModelMapper, ReportModel> implements IReportModelService {

}
