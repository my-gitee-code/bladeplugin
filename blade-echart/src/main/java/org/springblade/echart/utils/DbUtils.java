package org.springblade.echart.utils;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springblade.echart.entity.DataSource;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCallback;

import com.alibaba.druid.pool.DruidDataSource;

public class DbUtils {
	
	/**
	 * 获取数据库对象
	 * @param dataSource
	 * @return
	 */
	public static JdbcTemplate getJdbcTemplate(DataSource dataSource) {
		DruidDataSource managerDataSource = new DruidDataSource();
		managerDataSource.setDriverClassName(dataSource.getDriver());
		managerDataSource.setUsername(dataSource.getUsername());
		managerDataSource.setPassword(dataSource.getPassword());
		managerDataSource.setUrl(dataSource.getUrl());

		JdbcTemplate jdbcTemplate = new JdbcTemplate(managerDataSource);
		return jdbcTemplate;
	}
	
	/**
	 * 根据数据源获取当前数据库名
	 * @param jdbcTemplate
	 * @return
	 */
	public static String getDataBase(JdbcTemplate jdbcTemplate) {
		String database = null;
		List<Map<String, Object>> resultList = jdbcTemplate.queryForList("select database();");
		for (Map.Entry<String, Object> entry : resultList.get(0).entrySet()) {
			database = entry.getValue().toString();
			break;
		}
		return database;
	}
	
	/**
	 * 获取数据库所有表
	 * @param jdbcTemplate
	 * @return
	 */
	public static List<Map<String, String>> getTables(JdbcTemplate jdbcTemplate) {
		String schema = getDataBase(jdbcTemplate);
		String sql = String.format("select table_name,table_comment from information_schema.tables where table_schema='%s' order by table_name asc;", schema);
		return (List<Map<String, String>>)jdbcTemplate.execute(sql, new PreparedStatementCallback() {

			@Override
			public Object doInPreparedStatement(PreparedStatement ps) throws SQLException, DataAccessException {
				List<Map<String, String>> result = new ArrayList<>();
                ResultSet resultSet = ps.executeQuery();
                while (resultSet.next()) {
                	String name = resultSet.getString("table_name");
                	String comment = resultSet.getString("table_comment");
                	
                	Map<String, String> item = new HashMap<>();
                	item.put("name", name);
                	item.put("comment", comment);
                	result.add(item);
                }
				return result;
			}
			
		});
	}
	
	/**
	 * 获取表中所有字段
	 * @param jdbcTemplate
	 * @param table
	 * @return
	 */
	public static List<Map<String, String>> getFields(JdbcTemplate jdbcTemplate, String table) {
		String schema = getDataBase(jdbcTemplate);
		String sql = String.format("select column_name,data_type,character_maximum_length,numeric_precision,numeric_scale,is_nullable,extra,column_comment,column_type,column_default from information_schema.columns where table_schema='%s' and table_name='%s' order by ordinal_position asc,column_name asc;", schema, table);
		return (List<Map<String, String>>)jdbcTemplate.execute(sql, new PreparedStatementCallback() {

			@Override
			public Object doInPreparedStatement(PreparedStatement ps) throws SQLException, DataAccessException {
				List<Map<String, String>> result = new ArrayList<>();
                ResultSet resultSet = ps.executeQuery();
                while (resultSet.next()) {
                	String name = resultSet.getString("column_name");
                	String comment = resultSet.getString("column_comment");
                	
                	Map<String, String> item = new HashMap<>();
                	item.put("name", name);
                	item.put("comment", comment);
                	result.add(item);
                }
				return result;
			}
			
		});
	}
	
}
