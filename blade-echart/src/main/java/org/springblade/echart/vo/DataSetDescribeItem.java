package org.springblade.echart.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value="deployItem", description="数据集配置")
public class DataSetDescribeItem {
	
	@ApiModelProperty(value = "字段名")
	private String field;
	
	@ApiModelProperty(value = "标题")
	private String title;

	@ApiModelProperty(value = "字典配置地址")
	private String dictUrl;

	@ApiModelProperty(value = "java二次处理方法名")
	private String javaFunction;

	@ApiModelProperty(value = "前端js二次处理方法名")
	private String jsFunction;

	@ApiModelProperty(value = "图表类型")
	private String echartType;

}
