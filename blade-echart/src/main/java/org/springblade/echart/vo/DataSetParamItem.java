package org.springblade.echart.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value="DataSetParamItem", description="查询参数")
public class DataSetParamItem {

	@ApiModelProperty(value = "字段名")
	private String field;

	@ApiModelProperty(value = "名称")
	private String name;

	@ApiModelProperty(value = "描述")
	private String description;

	@ApiModelProperty(value = "默认值")
	private String defaultValue;
}
