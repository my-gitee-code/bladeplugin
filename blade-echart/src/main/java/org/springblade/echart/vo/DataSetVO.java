package org.springblade.echart.vo;

import java.util.Date;
import java.util.List;

import org.springblade.echart.entity.DataSet;
import org.springframework.beans.BeanUtils;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value="DataSetVO", description="数据集")
public class DataSetVO {
	
	private Integer id;
	
	@ApiModelProperty(value = "名称")
	private String name;

	@ApiModelProperty(value = "所属数据源ID")
	private Integer sourceId;
	
	@ApiModelProperty(value = "所属数据源名称")
	private String sourceName;

	@ApiModelProperty(value = "表名")
	private String tableName;

	@ApiModelProperty(value = "查询SQL")
	private String query;

	@ApiModelProperty(value = "描述")
	private String description;

	@ApiModelProperty(value = "配置，json数据")
	private List<DataSetDescribeItem> deploys;

	@ApiModelProperty(value = "查询参数，json数据")
	private List<DataSetParamItem> params;
	
	@ApiModelProperty(value = "状态：1=启用")
	private Integer status;

	@ApiModelProperty(value = "新增或修改时间")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
	private Date createTime;
	
	/**
	 * 转换成数据库对象
	 * @return
	 */
	public DataSet toDataSet() {
		DataSet dataSet = new DataSet();
		BeanUtils.copyProperties(this, dataSet);
		
		dataSet.setDeploy(JSON.toJSONString(getDeploys()));
		dataSet.setParam(JSON.toJSONString(getParams()));
		
		if (dataSet.getStatus() == null) {
			dataSet.setStatus(0);
		}
		if (dataSet.getCreateTime() == null) {
			dataSet.setCreateTime(new Date());
		}
		
		return dataSet;
	}

}
