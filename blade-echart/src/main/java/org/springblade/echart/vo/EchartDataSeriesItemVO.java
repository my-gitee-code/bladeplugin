package org.springblade.echart.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value="EchartDataSeriesItemVO", description="series数据项")
public class EchartDataSeriesItemVO {

	@ApiModelProperty(value = "名称")
	private String name;

	@ApiModelProperty(value = "图表类型")
	private String type;

	@ApiModelProperty(value = "单位")
	private String unit;

	@ApiModelProperty(value = "数据")
	private Object data;
	
}
