package org.springblade.echart.vo;

import java.util.Date;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;

import com.alibaba.fastjson.JSONArray;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value="EchartDataVO", description="图表数据")
public class EchartDataVO {
	
	@ApiModelProperty(value = "系统编号")
	private Integer id;
	
	@ApiModelProperty(value = "标题")
	private String name;

	@ApiModelProperty(value = "数据集ID")
	private Integer datasetId;

	@ApiModelProperty(value = "数据字段")
	private JSONArray dataFields;

	@ApiModelProperty(value = "统计字段")
	private String censusField;

	@ApiModelProperty(value = "单位")
	private String unit;
	
	@ApiModelProperty(value = "描述")
	private String description;
	
	@ApiModelProperty(value = "状态：1=启用")
	private Integer status;

	@ApiModelProperty(value = "新增或修改时间")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
	private Date createTime;

	@ApiModelProperty(value = "图表类型")
	private String type;

	@ApiModelProperty(value = "legend")
	private List<String> legends;

	@ApiModelProperty(value = "X轴")
	private List<String> xaxis;

	@ApiModelProperty(value = "数据")
	private Object series;
	
}
