package org.springblade.os;

import org.springblade.core.launch.BladeApplication;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class OsApplication {

    public static void main(String[] args) {
        BladeApplication.run("blade-os", OsApplication.class, args);
    }

    @Bean
    CommandLineRunner lookupTestService() {
        return args -> {
            System.out.println("ok");
        };
    }

}
