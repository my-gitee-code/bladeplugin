package org.springblade.os.commons;

import lombok.extern.slf4j.Slf4j;
import org.springblade.os.utils.SystemUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * 系统进程管理
 * @author zhixin
 */
@Slf4j
public class OsProcess {

    /**
     * 启动一个进程
     * @param command
     * @return
     */
    public static Boolean startProcess(List<String> command) {
        try {
            log.info("startProcess, {}", command);
            ProcessBuilder processBuilder = new ProcessBuilder(command);
            Process process = processBuilder.start();
//            process.waitFor(30, TimeUnit.SECONDS);

            return Boolean.TRUE;
        } catch (Exception e) {
            log.info("执行命令出错: {}", e);
        }
        return Boolean.FALSE;
    }

    /**
     * 启动一个进程
     * @param directory 工作目录: "/home/opt/workspace/java/flowable-6.8.0/wars"
     * @param command 启动参数: "java", "-jar", "flowable-ui.war", "--server.port=8101"
     * @return
     */
    public static Boolean startProcess(String directory, List<String> command) {
        try {
            log.info("startProcess, dir: {}, cmd: {}", directory, command);
            File file = new File(directory);
            if (file.exists()) {
                ProcessBuilder processBuilder = new ProcessBuilder(command);
                processBuilder.directory(file);
                Process process = processBuilder.start();
//                process.waitFor(30, TimeUnit.SECONDS);
                return Boolean.TRUE;
            }
        } catch (Exception e) {
            log.info("执行命令出错: {}", e);
        }
        return Boolean.FALSE;
    }

    /**
     * 结束一个进程
     * @param processName
     * @return
     */
    public static Boolean killProcess(String processName) {
        Integer pid = queryJavaProcessPid(processName);
        if (pid > 0) {
            List<String> command = new ArrayList<>();
            if (SystemUtils.isWindows()) {
                command = Arrays.asList(new String[]{ "taskkill", "/f", "/pid", String.valueOf(pid) });
            } else {
                command = Arrays.asList(new String[]{ "kill", "-9", String.valueOf(pid) });
            }
            return startProcess(command);
        }

        return Boolean.FALSE;
    }

    /**
     * windows查询服务状态
     * sc query mysql
     * @param serviceName
     * @return
     */
    public static String queryServiceState(String serviceName) {
        try {
            ProcessBuilder processBuilder = new ProcessBuilder("sc", "query", serviceName);
            Process process = processBuilder.start();
            BufferedReader inputReader = new BufferedReader(new InputStreamReader(process.getInputStream()));

            StringBuffer sb = new StringBuffer();
            String line = null;
            while ((line = inputReader.readLine()) != null) {
                sb = sb.append(line);
                if (line.toUpperCase().indexOf("STATE") != -1) {
                    String[] tmp = line.split(" ");
                    return tmp[tmp.length - 1];
                }
            }
            inputReader.close();
            log.info("queryServiceState\ncmd: sc query {}\nresponse: \n{}", serviceName, sb.toString());

            if (sb.toString().indexOf("指定的服务未安装") != -1) {
                return "未安装";
            }

            return sb.toString();
        } catch (Exception e) {
            log.info("执行命令出错: {}", e);
        }

        return "";
    }

    /**
     * windows查询服务状态
     * sc query mysql
     * net start | findstr /i mysql
     * @param serviceName
     * @param status 状态:stop,start
     * @return
     */
    public static Boolean queryServiceState(String serviceName, String status) {
        try {
            ProcessBuilder processBuilder = new ProcessBuilder("net", status, "|", "findstr", "/i", serviceName);
            Process process = processBuilder.start();
            BufferedReader inputReader = new BufferedReader(new InputStreamReader(process.getInputStream()));

            StringBuffer sb = new StringBuffer();
            String line = null;
            while ((line = inputReader.readLine()) != null) {
                sb = sb.append(line);
            }
            inputReader.close();

            log.info("queryServiceState\nresponse: \n{}", sb.toString());
            return !sb.toString().trim().isEmpty();
        } catch (Exception e) {
            log.info("执行命令出错: {}", e);
        }

        return Boolean.FALSE;
    }

    /**
     * windows显示正在运行的所有本地服务列表
     * net start
     * @return
     */
    public static List<String> queryServiceState() {
        List<String> services = new ArrayList<>();
        try {
            ProcessBuilder processBuilder = new ProcessBuilder("net", "start");
            Process process = processBuilder.start();
            BufferedReader inputReader = new BufferedReader(new InputStreamReader(process.getInputStream()));

            StringBuffer sb = new StringBuffer();
            String line = null;
            while ((line = inputReader.readLine()) != null) {
                sb = sb.append(line);
                services.add(line);
            }
            inputReader.close();
            log.info("queryServiceState\ncmd: net start\nresponse: \n{}", sb.toString());
        } catch (Exception e) {
            log.info("执行命令出错: {}", e);
        }

        return services;
    }

    /**
     * 查看当前系统的java进程:jps -l
     * @return
     */
    public static List<String> jpsL() {
        List<String> strs = new ArrayList<>();
        try {
            ProcessBuilder processBuilder = new ProcessBuilder("jps", "-l");
            Process process = processBuilder.start();
            BufferedReader inputReader = new BufferedReader(new InputStreamReader(process.getInputStream()));

            StringBuffer sb = new StringBuffer();
            String line = null;

            while ((line = inputReader.readLine()) != null) {
                sb = sb.append(line);
                String[] tmp = line.split(" ");
                if (tmp.length == 1) {
                    continue;
                }
                strs.add(tmp[1]);
            }

            inputReader.close();
            log.info("jpsL\ncmd: jps -l\nresponse: \n{}", sb.toString());
        } catch (Exception e) {
            log.info("执行命令出错: {}", e);
        }
        return strs;
    }

    /**
     * 查找进程PID
     * jps -l | findstr blade-os.jar
     * @param processName
     * @return
     */
    public static Integer queryJavaProcessPid(String processName) {
        try {
            ProcessBuilder processBuilder = new ProcessBuilder("jps", "-l");
            Process process = processBuilder.start();
            BufferedReader inputReader = new BufferedReader(new InputStreamReader(process.getInputStream()));

            StringBuffer sb = new StringBuffer();
            String line = null;
            while ((line = inputReader.readLine()) != null) {
                sb = sb.append(line);
                String[] tmp = line.split(" ");
                if (tmp.length == 1) {
                    continue;
                }
                if (tmp[1].equals(processName)) {
                    return Integer.parseInt(tmp[0]);
                }
            }
            inputReader.close();

            log.info("queryJavaProcessPid\ncmd: jps -l\nresponse: \n{}", sb.toString());
        } catch (Exception e) {
            log.info("执行命令出错: {}", e);
        }

        return 0;
    }
}
