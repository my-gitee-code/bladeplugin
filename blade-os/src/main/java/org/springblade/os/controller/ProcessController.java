package org.springblade.os.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springblade.core.mp.support.Condition;
import org.springblade.core.mp.support.Query;
import org.springblade.core.tool.api.R;
import org.springblade.os.commons.OsProcess;
import org.springblade.os.entity.Process;
import org.springblade.os.service.IProcessService;
import org.springblade.os.vo.ProcessVO;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Slf4j
@Controller
@RequestMapping("process")
@Api(tags = "系统进程")
public class ProcessController {

    @Resource
    IProcessService processService;

    @ResponseBody
    @PostMapping("{id}")
    @ApiOperation(value = "新增或修改")
    public R save(@PathVariable("id") Integer id, @RequestBody org.springblade.os.entity.Process process) {
        if (id == 0) {
            process.setId(null);
        } else {
            process.setId(id);
        }
        process.setCreateTime(new Date());
        return R.data(processService.saveOrUpdate(process));
    }

    @ResponseBody
    @DeleteMapping("{id}")
    @ApiOperation(value = "删除")
    public R delete(@PathVariable("id") Integer id) {
        org.springblade.os.entity.Process process = processService.getById(id);
        if (process != null) {
            process.setIsDeleted(1);
        }
        processService.updateById(process);
        return R.data("ok");
    }

    @ResponseBody
    @GetMapping("{id}")
    @ApiOperation(value = "详情")
    public R detail(@PathVariable("id") Integer id) {
        org.springblade.os.entity.Process process = processService.getById(id);
        if (process != null) {
            return R.data(process);
        }
        return R.fail("数据不存在");
    }

    @GetMapping("index")
    @ApiOperation(value = "分页查询")
    public String page(Model model, @RequestParam(required = false) Integer page, @RequestParam(required = false) Integer size) {
        Query query = new Query();
        if (page != null && page != 0) {
            query.setCurrent(page);
        } else {
            query.setCurrent(1);
        }
        if (size != null && size != 0) {
            query.setSize(size);
        } else {
            query.setSize(10);
        }

        IPage<Process> processPage = processService.page(Condition.getPage(query), new LambdaQueryWrapper<>());
        IPage<ProcessVO> processVOIPage = new Page<>();
        BeanUtils.copyProperties(processPage, processVOIPage);

        // 所有已启动的JAVA进程
        List<String> processList = OsProcess.jpsL();

        List<ProcessVO> processVOList = new ArrayList<>();
        processPage.getRecords().forEach(item -> {
            ProcessVO processVO = new ProcessVO();
            BeanUtils.copyProperties(item, processVO);

            // 查询进程状态
            if (processList.contains(item.getName())) {
                processVO.setRun(2);
            } else {
                processVO.setRun(0);
            }

            processVOList.add(processVO);
        });
        processVOIPage.setRecords(processVOList);

        model.addAttribute("title", "系统进程");
        model.addAttribute("data", processVOIPage);

        return "process/index";
    }

    @ResponseBody
    @GetMapping("start")
    @ApiOperation(value = "启动")
    public R start(@RequestParam Integer id) {
        org.springblade.os.entity.Process process = processService.getById(id);
        if (process == null) {
            return R.fail("进程不存在");
        }

        List<String> command = Arrays.asList(process.getCmd().split(" "));
        if (OsProcess.startProcess(process.getDirectory(), command)) {
            return R.success("正在启动");
        } else {
            return R.fail("操作失败");
        }
    }

    @ResponseBody
    @GetMapping("stop")
    @ApiOperation(value = "停止")
    public R stop(@RequestParam Integer id) {
        org.springblade.os.entity.Process process = processService.getById(id);
        if (process == null) {
            return R.fail("进程不存在");
        }

        if (OsProcess.killProcess(process.getName())) {
            return R.success("正在停止");
        } else {
            return R.fail("操作失败");
        }
    }
}
