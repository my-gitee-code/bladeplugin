package org.springblade.os.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springblade.core.mp.support.Condition;
import org.springblade.core.mp.support.Query;
import org.springblade.core.tool.api.R;
import org.springblade.os.commons.OsProcess;
import org.springblade.os.entity.Service;
import org.springblade.os.service.IServiceService;
import org.springblade.os.vo.ServiceVO;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Slf4j
@Controller
@RequestMapping("service")
@Api(tags = "系统服务")
public class ServiceController {

    @Resource
    IServiceService serviceService;

    @ResponseBody
    @PostMapping("{id}")
    @ApiOperation(value = "新增或修改")
    public R save(@PathVariable("id") Integer id, @RequestBody Service service) {
        if (id == 0) {
            service.setId(null);
        } else {
            service.setId(id);
        }
        service.setCreateTime(new Date());
        return R.data(serviceService.saveOrUpdate(service));
    }

    @ResponseBody
    @DeleteMapping("{id}")
    @ApiOperation(value = "删除")
    public R delete(@PathVariable("id") Integer id) {
        Service service = serviceService.getById(id);
        if (service != null) {
            service.setIsDeleted(1);
        }
        serviceService.updateById(service);
        return R.data("ok");
    }

    @ResponseBody
    @GetMapping("{id}")
    @ApiOperation(value = "详情")
    public R detail(@PathVariable("id") Integer id) {
        Service service = serviceService.getById(id);
        if (service != null) {
            return R.data(service);
        }
        return R.fail("数据不存在");
    }

    @GetMapping("index")
    @ApiOperation(value = "分页查询")
    public String page(Model model, @RequestParam(required = false) Integer page, @RequestParam(required = false) Integer size) {
        Query query = new Query();
        if (page != null && page != 0) {
            query.setCurrent(page);
        } else {
            query.setCurrent(1);
        }
        if (size != null && size != 0) {
            query.setSize(size);
        } else {
            query.setSize(10);
        }

        IPage<Service> servicePage = serviceService.page(Condition.getPage(query), new LambdaQueryWrapper<>());
        IPage<ServiceVO> serviceVOIPage = new Page<>();
        BeanUtils.copyProperties(servicePage, serviceVOIPage);

        // 所有已启动的服务
        List<String> serviceStatus = OsProcess.queryServiceState();

        List<ServiceVO> serviceVOList = new ArrayList();
        servicePage.getRecords().forEach(item -> {
            ServiceVO serviceVO = new ServiceVO();
            BeanUtils.copyProperties(item, serviceVO);

            if (serviceStatus.contains(item.getName())) {
                serviceVO.setRun(2);
            } else {
                String state = OsProcess.queryServiceState(item.getName());
                if (state.equals("STOPPED")) {
                    serviceVO.setRun(0);
                }
                else if (state.equals("RUNNING")) {
                    serviceVO.setRun(2);
                }
            }

            serviceVOList.add(serviceVO);
        });
        serviceVOIPage.setRecords(serviceVOList);

        model.addAttribute("title", "系统服务");
        model.addAttribute("data", serviceVOIPage);

        return "service/index";
    }

    @ResponseBody
    @GetMapping("start")
    @ApiOperation(value = "启动")
    public R start(@RequestParam Integer id) {
        Service service = serviceService.getById(id);
        if (service == null) {
            return R.fail("服务不存在");
        }

        List<String> command = Arrays.asList(new String[]{ "net", "start", service.getName() });
        OsProcess.startProcess(command);

        return R.success("正在启动");
    }

    @ResponseBody
    @GetMapping("stop")
    @ApiOperation(value = "停止")
    public R stop(@RequestParam Integer id) {
        Service service = serviceService.getById(id);
        if (service == null) {
            return R.fail("服务不存在");
        }

        List<String> command = Arrays.asList(new String[]{ "net", "stop", service.getName() });
        OsProcess.startProcess(command);

        return R.success("正在停止");
    }
}
