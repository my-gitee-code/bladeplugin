package org.springblade.os.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
@TableName("blade_os_process")
@ApiModel(value = "Process", description = "系统进程")
public class Process {

    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Integer id;

    @ApiModelProperty(value = "名称")
    private String name;

    @ApiModelProperty(value = "目录")
    private String directory;

    @ApiModelProperty(value = "完整命令及参数")
    private String cmd;

    @ApiModelProperty(value = "描述")
    private String description;

    @ApiModelProperty(value = "端口")
    private Integer port;

    @ApiModelProperty(value = "状态:1=启用,2=禁用", example = "0")
    private Integer status;

    @ApiModelProperty(value = "是否已删除", example = "0")
    private Integer isDeleted;

    @ApiModelProperty(value = "新增或修改时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;

}
