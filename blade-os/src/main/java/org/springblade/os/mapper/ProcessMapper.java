package org.springblade.os.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface ProcessMapper extends BaseMapper<org.springblade.os.entity.Process> {
}
