package org.springblade.os.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springblade.os.entity.Service;

public interface ServiceMapper extends BaseMapper<Service> {
}
