package org.springblade.os.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.springblade.os.entity.Service;

public interface IServiceService extends IService<Service> {
}
