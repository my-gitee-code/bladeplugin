package org.springblade.os.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springblade.os.mapper.ProcessMapper;
import org.springblade.os.service.IProcessService;
import org.springframework.stereotype.Service;

@Service
public class ProcessServiceImpl extends ServiceImpl<ProcessMapper, org.springblade.os.entity.Process> implements IProcessService {
}
