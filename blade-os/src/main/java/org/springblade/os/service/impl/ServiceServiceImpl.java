package org.springblade.os.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springblade.os.entity.Service;
import org.springblade.os.mapper.ServiceMapper;
import org.springblade.os.service.IServiceService;

@org.springframework.stereotype.Service
public class ServiceServiceImpl extends ServiceImpl<ServiceMapper, Service> implements IServiceService {
}
