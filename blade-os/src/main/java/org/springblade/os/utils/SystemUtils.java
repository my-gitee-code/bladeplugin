package org.springblade.os.utils;

public class SystemUtils {

    public static boolean isWindows() {
        String osName = System.getProperty("os.name");
        return osName != null && osName.startsWith("Windows");
    }

    public static boolean isMacOs() {
        String osName = System.getProperty("os.name");
        return osName != null && osName.startsWith("Mac");
    }

    public static boolean isLinux() {
        String osName = System.getProperty("os.name");
        return (osName != null && osName.startsWith("Linux")) || (!isWindows() && !isMacOs());
    }

}
