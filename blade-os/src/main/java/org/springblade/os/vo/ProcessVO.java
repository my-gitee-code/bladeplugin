package org.springblade.os.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "Process", description = "系统进程")
public class ProcessVO extends org.springblade.os.entity.Process {

    @ApiModelProperty(value = "运行状态:0=已停止;1=启动中;2=已启动;3=停止中")
    private Integer run;

}
