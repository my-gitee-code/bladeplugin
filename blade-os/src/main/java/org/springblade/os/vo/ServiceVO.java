package org.springblade.os.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springblade.os.entity.Service;

@Data
@ApiModel(value = "Service", description = "系统服务")
public class ServiceVO extends Service {

    @ApiModelProperty(value = "运行状态:0=已停止;1=启动中;2=已启动;3=停止中")
    private Integer run;

}
