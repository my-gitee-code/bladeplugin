package org.springblade.pay.event;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zhixin
 * 自定义退款到帐事件
 * eg:
 * @Slf4j
 * @Component
 * public class RefundEventListener {
 *  @EventListener
 *  public void handleRefundEvent(RefundEvent event) {
 *      log.info(event);
 *  }
 * }
 */
@Data
public class RefundEvent {

    @ApiModelProperty(value = "订单号")
    private String outTradeNo;

    @ApiModelProperty(value = "交易金额,单位:分", example = "1")
    private Integer amount;

    @ApiModelProperty(value = "附加信息")
    private String attach;

    @ApiModelProperty(value = "扩展参数")
    private String params;

    @ApiModelProperty(value = "操作员")
    private Long opUserId;

    /**
     * 自定义退款到帐事件
     * @param outTradeNo 订单号
     * @param amount 交易金额,单位:分
     * @param params 附加信息
     * @param params 扩展参数
     * @param opUserId 操作员
     */
    public RefundEvent(String outTradeNo, Integer amount, String attach, String params, Long opUserId) {
        this.outTradeNo = outTradeNo;
        this.amount = amount;
        this.attach = attach;
        this.params = params;
        this.opUserId = opUserId;
    }
}
