package org.springblade.pay.feign;

import org.springframework.cloud.openfeign.FeignClient;

/**
 * @author zhixin
 */
@FeignClient(name = "blade-pay")
public interface PayClient {
}
