package org.springblade.pay.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 支付参数
 * @author zhixin
 */
@Data
@ApiModel(value = "支付参数")
public class PaymentVO {

    @ApiModelProperty(value = "支付通道编号", example = "1", required = true)
    private Integer channelId;

    @ApiModelProperty(value = "订单号", required = true)
    private String outTradeNo;

    @ApiModelProperty(value = "交易金额,单位:分", example = "1", required = true)
    private Integer amount;

    @ApiModelProperty(value = "附加信息")
    private String attach;

    @ApiModelProperty(value = "扩展参数,json格式")
    private String params;

    @ApiModelProperty(value = "备注", required = true)
    private String description;

    @ApiModelProperty(value = "支付者的微信openid", required = true)
    private String openid;

    @ApiModelProperty(value = "操作员")
    private Long opUserId;

    @ApiModelProperty(value = "是否指定分账", example = "0")
    private Boolean profitSharing;
}
