package cn.swiftpass.util;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * 汇旺财支付
 * @author zhixin
 */
@Slf4j
public class PayUtil {

	public static String ip = "";
	public static String notifyHost = "https://www.pzk.cn/api/";
	public static String reqUrl = "https://pay.hstypay.com/v2/pay/gateway";

	/**
	 * 初始化支付
	 * @param outTradeNo 商户订单号
	 * @param body 商品描述
	 * @param price 总金额，以分为单位
	 * @param openId 用户openid
	 * @param appid 小程序的AppID
	 * @param mchId 门店编号，由平台分配
	 * @param attach 附加信息
	 * @param key 商户MD5密钥信息
	 * @param notifyUrl 通知地址
	 * @return
	 * @throws Exception
	 */
	public JSONObject pay(String outTradeNo, String body, Integer price, String openId, String appid, String mchId, String attach, String key, String notifyUrl) throws Exception {
		return pay(outTradeNo, body, price, openId, appid, mchId, attach, key, null, notifyUrl);
	}

	/**
	 * 初始化支付
	 * @param outTradeNo 商户订单号
	 * @param body 商品描述
	 * @param price 总金额，以分为单位
	 * @param openId 用户openid
	 * @param appid 小程序的AppID
	 * @param mchId 门店编号，由平台分配
	 * @param attach 附加信息
	 * @param key 商户MD5密钥信息
	 * @param profitShareInfos 分账信息
	 * @param notifyUrl 通知地址
	 * @return
	 * @throws Exception
	 */
	public JSONObject pay(String outTradeNo, String body, Integer price, String openId, String appid, String mchId, String attach, String key, List<ProfitShareInfos> profitShareInfos, String notifyUrl) throws Exception {

		if (ip == null || ip.isEmpty()) {
			ip = getPublicNetworkIp();
		}

		SortedMap<String, String> map = new TreeMap();
		map.put("service", "pay.weixin.jspay");
		map.put("version", "2.0");
		map.put("charset", "UTF-8");
		map.put("sign_type", "MD5");

		map.put("mch_id", mchId);
		map.put("is_raw", "1");
		map.put("is_minipg", "1");

		map.put("out_trade_no", outTradeNo);
		map.put("body", body);

		map.put("sub_openid", openId);
		map.put("sub_appid", appid);

		if (profitShareInfos != null) {
			map.put("profit_share_infos", JSONObject.toJSONString(profitShareInfos));
		}

		map.put("attach", attach);
		map.put("total_fee", String.format("%d", price));

		map.put("mch_create_ip", ip);
		map.put("notify_url", String.format("%s%s", notifyHost, notifyUrl));
		map.put("nonce_str", String.valueOf(System.currentTimeMillis()));

		Map<String, String> params = SignUtils.paraFilter(map);
		StringBuilder buf = new StringBuilder((params.size() + 1) * 10);
		SignUtils.buildPayParams(buf, params, false);
		String preStr = buf.toString();
		String sign_type = map.get("sign_type");

		map.put("sign", SignUtil.getSign("", key, sign_type, preStr));

		Map<String, String> result = doPost(map, key);
		JSONObject jsonObject = JSONObject.parseObject(result.get("pay_info"));
		jsonObject.put("packageVal", jsonObject.getString("package"));

		return jsonObject;
	}

	/**
	 * 支付查询
	 * @param outTradeNo 商户订单号
	 * @param mchId 门店编号，由平台分配
	 * @param key 商户MD5密钥信息
	 * @return
	 * @throws Exception
	 */
	public String payQuery(String outTradeNo, String mchId, String key) throws Exception {
		String platPublicKey = "";
		String sign_type = "MD5";

		SortedMap<String, String> map = new TreeMap();
		map.put("service", "unified.trade.query");
		map.put("version", "2.0");
		map.put("charset", "UTF-8");
		map.put("sign_type", sign_type);
		map.put("mch_id", mchId);
		map.put("out_trade_no", outTradeNo);
		map.put("nonce_str", String.valueOf(System.currentTimeMillis()));

		Map<String, String> params = SignUtils.paraFilter(map);
		StringBuilder buf = new StringBuilder((params.size() + 1) * 10);
		SignUtils.buildPayParams(buf, params, false);
		String preStr = buf.toString();
		map.put("sign", SignUtil.getSign(platPublicKey, key, sign_type, preStr));

		Map<String, String> result = doPost(map, key);
		return result.get("transaction_id");
	}

	/**
	 * 申请退款
	 * @param outTradeNo 商户订单号
	 * @param outRefundNo 商户退款单号
	 * @param total 总金额
	 * @param price 退款金额
	 * @param mchId 门店编号，由平台分配
	 * @param key 商户MD5密钥信息
	 * @param opUserId 操作员帐号,默认为商户号
	 * @return
	 * @throws Exception
	 */
	public String refund(String outTradeNo, String outRefundNo, Integer total, Integer price, String mchId, String key, Long opUserId) throws Exception {
		String platPublicKey = "";
		String sign_type = "MD5";

		SortedMap<String, String> map = new TreeMap();
		map.put("service", "unified.trade.refund");
		map.put("version", "2.0");
		map.put("charset", "UTF-8");
		map.put("sign_type", sign_type);
		map.put("mch_id", mchId);
		map.put("out_trade_no", outTradeNo);
		map.put("out_refund_no", outRefundNo);
		map.put("total_fee", String.format("%d", total));
		map.put("refund_fee", String.format("%d", price));
		map.put("op_user_id", String.format("%d", opUserId));

		map.put("nonce_str", String.valueOf(System.currentTimeMillis()));

		Map<String, String> params = SignUtils.paraFilter(map);
		StringBuilder buf = new StringBuilder((params.size() + 1) * 10);
		SignUtils.buildPayParams(buf, params, false);
		String preStr = buf.toString();
		map.put("sign", SignUtil.getSign(platPublicKey, key, sign_type, preStr));

		Map<String, String> result = doPost(map, key);
		return result.get("transaction_id");
	}

	/**
	 * 退款查询
	 * @param outTradeNo 商户订单号
	 * @param outRefundNo 商户退款单号
	 * @param mchId 门店编号，由平台分配
	 * @param key 商户MD5密钥信息
	 * @return
	 * @throws Exception
	 */
	public String refundQuery(String outTradeNo, String outRefundNo, String mchId, String key) throws Exception {
		String platPublicKey = "";
		String sign_type = "MD5";

		SortedMap<String, String> map = new TreeMap();
		map.put("service", "unified.trade.query");
		map.put("version", "2.0");
		map.put("charset", "UTF-8");
		map.put("sign_type", sign_type);
		map.put("mch_id", mchId);
		map.put("out_trade_no", outTradeNo);
		map.put("out_refund_no", outRefundNo);
		map.put("nonce_str", String.valueOf(System.currentTimeMillis()));

		Map<String, String> params = SignUtils.paraFilter(map);
		StringBuilder buf = new StringBuilder((params.size() + 1) * 10);
		SignUtils.buildPayParams(buf, params, false);
		String preStr = buf.toString();
		map.put("sign", SignUtil.getSign(platPublicKey, key, sign_type, preStr));

		Map<String, String> result = doPost(map, key);
		return result.get("transaction_id");
	}

	private Map<String, String> doPost(SortedMap<String, String> map, String key) throws Exception {
		log.info("向汇旺财发起请求, {}", map);
		HttpPost httpPost = new HttpPost(reqUrl);
		StringEntity entityParams = new StringEntity(XmlUtils.parseXML(map), "utf-8");
		httpPost.setEntity(entityParams);
		httpPost.setHeader("Content-Type", "text/xml;utf-8");
		CloseableHttpClient client = HttpClients.createDefault();
		CloseableHttpResponse response = client.execute(httpPost);
		if (response != null && response.getEntity() != null) {
			Map<String, String> resultMap = XmlUtils.toMap(EntityUtils.toByteArray(response.getEntity()), "utf-8");
			String reSign = resultMap.get("sign");
			String res = XmlUtils.toXml(resultMap);
			log.info("汇旺财响应, {}", res);

			if (resultMap.containsKey("sign") && !SignUtil.verifySign("", key, reSign, map.get("sign_type"), resultMap)) {
				throw new Exception("签名错误");
			} else {
				if ("0".equals(resultMap.get("status")) && "0".equals(resultMap.get("result_code"))) {
					return resultMap;
				} else {
					if (resultMap.containsKey("err_msg")) {
						throw new Exception(resultMap.get("err_msg"));
					}
					throw new Exception(resultMap.get("message"));
				}
			}
		}

		if (response != null) {
			try {
				response.close();
			} catch (Exception e) {
			}
		}
		if (client != null) {
			try {
				client.close();
			} catch (Exception e) {
			}
		}

		return null;
	}

	/**
	 * 获取公网IP
	 * @return
	 */
	public static String getPublicNetworkIp() {
		try {
			Document document = Jsoup.connect("https://2023.ip138.com/").get();
			String ip = document.getElementsByTag("a").get(0).text().trim();
			return ip;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "127.0.0.1";
	}
}
