package cn.swiftpass.util;

import lombok.Data;

/**
 * 分账信息
 * @author zhixin
 */
@Data
public class ProfitShareInfos {
	/**
	 * 分账接收方(商户号、支付宝账号、微信openid)
	 */
	private String trans_in;

	/**
	 * 商户分账交易号
	 */
	private String trans_no;

	/**
	 * 分账金额,分为单位
	 */
	private Integer amount;

	/**
	 * 描述
	 */
	private String desc;
}
