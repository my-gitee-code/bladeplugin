package org.springblade.pay;

import org.springblade.core.cloud.feign.EnableBladeFeign;
import org.springblade.core.launch.BladeApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 支付模块
 * @author zhixin
 */
@EnableBladeFeign
@SpringBootApplication
public class PayApplication {

    public static void main(String[] args) {
        BladeApplication.run("blade-pay", PayApplication.class, args);
    }

}
