package org.springblade.pay.builder;

import org.springblade.pay.entity.Channel;
import org.springblade.pay.builder.template.*;

public class PayBuilder {

    public PayTemplate template(Channel channel) {
        if (channel.getController().equals("HstPayController")) {
            return new HstyTemplate();
        }
        else if (channel.getController().equals("WechatController")) {
            return new WechatTemplate();
        }
        else if (channel.getController().equals("WechatH5Controller")) {
            return new WechatH5Template();
        }
        else if (channel.getController().equals("WechatJsapiController")) {
            return new WechatJsapiTemplate();
        }
        return null;
    }

}
