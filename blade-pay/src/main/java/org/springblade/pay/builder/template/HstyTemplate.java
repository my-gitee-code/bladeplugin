package org.springblade.pay.builder.template;

import cn.swiftpass.util.PayUtil;
import org.springblade.pay.entity.Channel;
import org.springblade.pay.entity.Finance;

import java.util.Date;

public class HstyTemplate implements PayTemplate {

    @Override
    public Finance paymentQuery(Channel channel, Finance finance) throws Exception {
        return null;
    }

    @Override
    public Finance refundQuery(Channel channel, Finance finance) throws Exception {
        String transactionId = new PayUtil().refundQuery(finance.getOutTradeNo(), finance.getOutRefundNo(), channel.getMerchantId(), channel.getAppKey());

        Finance data = new Finance();
        data.setTransactionId(transactionId);
        data.setStatus(3);
        data.setUpdateTime(new Date());

        return data;
    }
}
