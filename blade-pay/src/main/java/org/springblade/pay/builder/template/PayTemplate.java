package org.springblade.pay.builder.template;

import org.springblade.pay.entity.Channel;
import org.springblade.pay.entity.Finance;

public interface PayTemplate {

    /**
     * 查询支付结果
     * @param channel
     * @param finance
     * @return
     */
    Finance paymentQuery(Channel channel, Finance finance) throws Exception;

    /**
     * 查询退款结果
     * @param channel
     * @param finance
     * @return
     * @throws Exception
     */
    Finance refundQuery(Channel channel, Finance finance) throws Exception;
}
