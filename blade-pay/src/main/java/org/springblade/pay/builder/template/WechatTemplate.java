package org.springblade.pay.builder.template;

import com.wechat.pay.java.core.Config;
import com.wechat.pay.java.core.RSAAutoCertificateConfig;
import com.wechat.pay.java.service.refund.RefundService;
import com.wechat.pay.java.service.refund.model.QueryByOutRefundNoRequest;
import com.wechat.pay.java.service.refund.model.Refund;
import org.springblade.pay.entity.Channel;
import org.springblade.pay.entity.Finance;

import java.text.SimpleDateFormat;
import java.util.Date;

public class WechatTemplate implements PayTemplate {

    /**
     * 微信支付设置
     * @param channel
     * @return
     * @throws Exception
     */
    public Config createConfig(Channel channel) throws Exception {
        if (channel == null) {
            throw new Exception("未设置支付通道");
        }
        if (channel.getMerchantId() == null || channel.getMerchantId().isEmpty()) {
            throw new Exception("微信支付商户号不能为空");
        }
        if (channel.getPrivateKeyPath() == null || channel.getPrivateKeyPath().isEmpty()) {
            throw new Exception("微信私匙不能为空");
        }
        if (channel.getMerchantSerial() == null || channel.getMerchantSerial().isEmpty()) {
            throw new Exception("微信秘匙不能为空");
        }
        if (channel.getAppKey() == null || channel.getAppKey().isEmpty()) {
            throw new Exception("微信APIv3密钥不能为空");
        }

        Config config = new RSAAutoCertificateConfig.Builder()
                .merchantId(channel.getMerchantId())
                .privateKeyFromPath(channel.getPrivateKeyPath())
                .merchantSerialNumber(channel.getMerchantSerial())
                .apiV3Key(channel.getAppKey())
                .build();
        return config;
    }

    @Override
    public Finance paymentQuery(Channel channel, Finance finance) throws Exception {
        return null;
    }

    @Override
    public Finance refundQuery(Channel channel, Finance finance) throws Exception {
        Config config = createConfig(channel);

        QueryByOutRefundNoRequest request = new QueryByOutRefundNoRequest();
        request.setOutRefundNo(finance.getOutRefundNo());

        RefundService refundService = new RefundService.Builder().config(config).build();
        Refund refund = refundService.queryByOutRefundNo(request);

        if (refund.getStatus().equals("SUCCESS")) {
            Date payTime;
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
            try {
                payTime = sdf.parse(refund.getSuccessTime());
            } catch (Exception e) {
                payTime = new Date();
            }

            Finance data = new Finance();
            data.setTransactionId(refund.getTransactionId());
            data.setStatus(3);
            data.setUpdateTime(payTime);
            return data;
        }

        return null;
    }
}
