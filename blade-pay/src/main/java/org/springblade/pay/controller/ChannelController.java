package org.springblade.pay.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springblade.core.mp.support.Condition;
import org.springblade.core.mp.support.Query;
import org.springblade.core.tool.api.R;
import org.springblade.pay.entity.Channel;
import org.springblade.pay.service.IChannelService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @author zhixin
 */
@Slf4j
@Controller
@RequestMapping("channel")
@Api(tags = "支付通道")
public class ChannelController {

    @Resource
    IChannelService channelService;

    @ResponseBody
    @PostMapping("{id}")
    @ApiOperation(value = "新增")
    public R add(@PathVariable("id") Integer id, @RequestBody Channel channel) {
        if (id == 0) {
            channelService.save(channel);
        } else {
            channel.setId(id);
            channelService.updateById(channel);
        }
        return R.data(channel);
    }

    @ResponseBody
    @DeleteMapping("{id}")
    @ApiOperation(value = "删除")
    public R delete(@PathVariable("id") Integer id) {
        Channel channel = channelService.getById(id);
        if (channel == null) {
            return R.fail("支付通道不存在");
        }

        channel.setIsDeleted(1);
        channelService.updateById(channel);
        return R.success("数据已删除");
    }

    @ResponseBody
    @GetMapping("{id}")
    @ApiOperation(value = "详情")
    public R detail(@PathVariable("id") Integer id) {
        Channel channel = channelService.getById(id);
        return R.data(channel);
    }

    @GetMapping("page")
    @ApiOperation(value = "分页查询")
    public String page(Model model, @RequestParam(required = false) Date startTime, @RequestParam(required = false) Date endTime, @RequestParam(required = false) Integer page, @RequestParam(required = false) Integer size) {
        LambdaQueryWrapper<Channel> lqw = new LambdaQueryWrapper<>();
        if (startTime != null) {
            lqw.ge(Channel::getCreateTime, startTime);
        }
        if (endTime != null) {
            lqw.le(Channel::getCreateTime, endTime);
        }
        lqw.orderByDesc(Channel::getCreateTime);

        Query query = new Query();
        if (page != null && page != 0) {
            query.setCurrent(page);
        } else {
            query.setCurrent(1);
        }
        if (size != null && size != 0) {
            query.setSize(size);
        } else {
            query.setSize(10);
        }

        IPage<Channel> channelPage = channelService.page(Condition.getPage(query), lqw);

        model.addAttribute("title", "支付通道");
        model.addAttribute("data", channelPage);

        return "channel/index";
    }

}
