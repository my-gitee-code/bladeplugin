package org.springblade.pay.controller;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springblade.core.excel.util.ExcelUtil;
import org.springblade.core.mp.support.Condition;
import org.springblade.core.mp.support.Query;
import org.springblade.core.tool.api.R;
import org.springblade.core.tool.utils.DateUtil;
import org.springblade.pay.entity.Finance;
import org.springblade.pay.mapper.FinanceMapper;
import org.springblade.pay.service.IFinanceService;
import org.springblade.pay.vo.FinanceSumVO;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;

/**
 * @author zhixin
 */
@Slf4j
@Controller
@RequestMapping("finance")
@Api(tags = "交易查询")
public class FinanceController {

    @Resource
    IFinanceService financeService;
    @Resource
    FinanceMapper financeMapper;

    @ResponseBody
    @PostMapping("{id}")
    @ApiOperation(value = "新增")
    public R add(@PathVariable("id") Long id, @RequestBody Finance finance) {
        return R.success("success");
    }

    @ResponseBody
    @DeleteMapping("{id}")
    @ApiOperation(value = "删除")
    public R delete(@PathVariable("id") Long id) {
        return R.success("success");
    }

    @ResponseBody
    @GetMapping("{id}")
    @ApiOperation(value = "详情")
    public R detail(@PathVariable("id") Long id) {
        Finance finance = financeService.getById(id);
        return R.data(finance);
    }

    @GetMapping("page")
    @ApiOperation(value = "分页查询")
    public String page(Model model, @RequestParam(required = false) Date startTime, @RequestParam(required = false) Date endTime, @RequestParam(required = false) Integer transactionType, @RequestParam(required = false) Integer page, @RequestParam(required = false) Integer size) {
        LambdaQueryWrapper<Finance> lqw = new LambdaQueryWrapper<>();
        if (startTime != null) {
            lqw.ge(Finance::getCreateTime, startTime);
        }
        if (endTime != null) {
            lqw.le(Finance::getCreateTime, endTime);
        }
        if (transactionType != null && (transactionType == 1 || transactionType == 2)) {
            lqw.eq(Finance::getTransactionType, transactionType);
        }
        lqw.orderByDesc(Finance::getCreateTime);

        Query query = new Query();
        if (page != null && page != 0) {
            query.setCurrent(page);
        } else {
            query.setCurrent(1);
        }
        if (size != null && size != 0) {
            query.setSize(size);
        } else {
            query.setSize(10);
        }

        IPage<Finance> financePage = financeService.page(Condition.getPage(query), lqw);

        model.addAttribute("title", "交易记录");
        model.addAttribute("data", financePage);

        return "finance/index";
    }

    @GetMapping("export")
    @ApiOperation(value = "导出")
    public void export(HttpServletResponse response, @RequestParam(required = false) Date startTime, @RequestParam(required = false) Date endTime, @RequestParam(required = false) Integer transactionType) {
        LambdaQueryWrapper<Finance> lqw = new LambdaQueryWrapper<>();
        if (startTime != null) {
            lqw.ge(Finance::getCreateTime, startTime);
        }
        if (endTime != null) {
            lqw.le(Finance::getCreateTime, endTime);
        }
        if (transactionType != null && (transactionType == 1 || transactionType == 2)) {
            lqw.eq(Finance::getTransactionType, transactionType);
        }
        lqw.orderByDesc(Finance::getCreateTime);

        Query query = new Query();
        query.setCurrent(1);
        query.setSize(65535);

        Page<Finance> financePage = new Page<>();
        financePage = financeService.page(financePage, lqw);

        ExcelUtil.export(response, "交易明细" + DateUtil.time(), "交易明细", financePage.getRecords(), Finance.class);
    }

    @ResponseBody
    @GetMapping("pay/{outTradeNo}")
    @ApiOperation(value = "查询交易是否成功")
    public R queryPay(@PathVariable("outTradeNo") String outTradeNo) {
        Finance finance = financeService.getByOutTradeNo(outTradeNo, 1);
        if (finance == null) {
            return R.fail("交易记录不存在");
        }

        return R.data(finance);
    }

    @GetMapping("refund/{outTradeNo}")
    @ApiOperation(value = "查询退款申请是否成功")
    public R queryRefund(@PathVariable("outTradeNo") String outTradeNo) {
        Finance finance = financeService.getByOutTradeNo(outTradeNo, 2);
        if (finance == null) {
            return R.fail("退款记录不存在");
        }

        return R.data(finance);
    }

    @GetMapping("report")
    @ApiOperation(value = "报表")
    public String report(Model model) {
        model.addAttribute("title", "资金统计");
        return "finance/report";
    }

    @ResponseBody
    @GetMapping("statistics")
    @ApiOperation(value = "统计分析")
    public R statisticAnalysis(@RequestParam Date startTime, @RequestParam Date endTime) {
        JSONObject jsonObject = new JSONObject();

        FinanceSumVO financeSumAllVO = financeMapper.summary(1, startTime, endTime);
        FinanceSumVO financeSumRefundVO = financeMapper.summary(2, startTime, endTime);

        JSONObject summary = new JSONObject();
        summary.put("money", financeSumAllVO.getAmount());
        summary.put("brokerage", financeSumAllVO.getBrokerage());
        summary.put("total", financeSumAllVO.getTotal());
        summary.put("refund", financeSumRefundVO.getAmount());
        jsonObject.put("summary", summary);

        return R.data(jsonObject);
    }
}
