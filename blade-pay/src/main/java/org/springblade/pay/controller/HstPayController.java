package org.springblade.pay.controller;

import cn.swiftpass.util.PayUtil;
import cn.swiftpass.util.SignUtil;
import cn.swiftpass.util.XmlUtils;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springblade.core.tool.api.R;
import org.springblade.pay.endpoint.PayEndpoint;
import org.springblade.pay.entity.Channel;
import org.springblade.pay.entity.Finance;
import org.springblade.pay.event.PaidEvent;
import org.springblade.pay.event.RefundEvent;
import org.springblade.pay.service.IChannelService;
import org.springblade.pay.service.IFinanceService;
import org.springblade.pay.vo.PaymentVO;
import org.springblade.pay.vo.TransactionVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * 汇旺财支付
 * @author zhixin
 */
@Slf4j
@RestController
@RequestMapping("hst")
@Api(tags = "汇旺财支付接口")
public class HstPayController {

    @Value("${blade.http.api}")
    private String host = "https://www.pzk.cn/api";
    @Value("${spring.application.name}")
    private String serverName;
    @Resource
    IFinanceService financeService;
    @Resource
    IChannelService channelService;
    @Resource
    PayEndpoint payEndpoint;
    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    @PostMapping("payment/submit")
    @ApiOperation(value = "发起支付")
    public R<TransactionVO> paymentSubmit(@RequestHeader HttpHeaders headers, @RequestBody PaymentVO paymentVO) {
        try {
            String notifyUrl = String.format("%s/%s/payment/notify", host, serverName);

            Channel channel = channelService.getById(paymentVO.getChannelId());
            if (channel == null) {
                return R.fail("未设置支付通道");
            }

            Finance finance = financeService.beforehandPay(paymentVO, "汇旺财", channel.getBrokerage(), paymentVO.getOpUserId());
            JSONObject jsonObject = new PayUtil().pay(paymentVO.getOutTradeNo(), paymentVO.getDescription(), paymentVO.getAmount(), paymentVO.getOpenid(), channel.getAppid(), channel.getMerchantId(), paymentVO.getAttach(), channel.getAppKey(), notifyUrl);
            finance.setStatus(1);
            financeService.updateById(finance);

            TransactionVO transactionVO = new TransactionVO();
            transactionVO.setOutTradeNo(paymentVO.getOutTradeNo());
            transactionVO.setAppid(channel.getAppid());
            transactionVO.setTimeStamp(jsonObject.getString("timeStamp"));
            transactionVO.setNonceStr(jsonObject.getString("nonceStr"));
            transactionVO.setPackageVal(jsonObject.getString("packageVal"));
            transactionVO.setSignType("RSA");
            transactionVO.setPaySign(jsonObject.getString("paySign"));

            return R.data(transactionVO);
        } catch (Exception e) {
            return R.fail(e.getCause() != null ? e.getCause().getMessage() : e.getMessage());
        }
    }

    /**
     * 支付回调通知,需要放开xss检测
     * @param request
     * @return
     */
    @PostMapping("payment/notify")
    @ApiOperation(value = "支付回调通知")
    public String paymentNotify(HttpServletRequest request) {
        try {
            request.setCharacterEncoding("utf-8");
            String resString = XmlUtils.parseRequst(request);
            log.info("汇旺财支付信息, {}", resString);

            if(resString != null && !"".equals(resString)){
                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
                Map<String, String> map = XmlUtils.toMap(resString.getBytes(), "utf-8");
                String outTradeNo = map.get("out_trade_no");

                Finance finance = financeService.getByOutTradeNo(outTradeNo, 1);
                if (finance != null && finance.getStatus() != 3) {
                    Channel channel = channelService.getById(finance.getChannelId());
                    if (SignUtil.verifySign("", channel.getAppKey(), map.get("sign"), map.get("sign_type"), map)) {
                        if (map.get("status").equals("0") && map.get("result_code").equals("0")) {
                            try {
                                Date payTime = sdf.parse(map.get("time_end"));
                                financeService.transactionResults(finance, map.get("transaction_id"), 3, null, payTime);
                                applicationEventPublisher.publishEvent(new PaidEvent(outTradeNo, finance.getAmount(), finance.getAttach(), finance.getParams(), finance.getOpUserId()));
                            } catch (Exception e) {
                                financeService.transactionResults(finance, map.get("transaction_id"), 3, null, null);
                            }
                            payEndpoint.send(String.format("%d", finance.getOpUserId()), finance.getOutTradeNo(), 0);
                        } else {
                            financeService.transactionResults(finance, null, 2, map.get("message"), null);
                            payEndpoint.send(String.format("%d", finance.getOpUserId()), finance.getOutTradeNo(), 1);
                            log.info("交易失败:{}", map.get("message"));
                        }
                    } else {
                        log.info("签名错误:{}", map);
                    }
                }
            }
        } catch (Exception e) {
            log.info("处理支付信息失败, {}", e);
        }
        return "success";
    }

    @PostMapping("payment/prorate")
    @ApiOperation(value = "发起支付-分帐")
    public void paymentProrate() {

    }

    @GetMapping("payment/query/{outTradeNo}")
    @ApiOperation(value = "查询支付结果")
    public R<Boolean> paymentQuery(@PathVariable("outTradeNo") String outTradeNo) {
        try {
            Finance finance = financeService.getByOutTradeNo(outTradeNo, 1);
            if (finance == null) {
                return R.fail("交易记录不存在");
            }

            if (finance.getStatus() != 3) {
                Channel channel = channelService.getById(finance.getChannelId());
                if (channel == null) {
                    return R.fail("未设置支付通道");
                }

                String transactionId = new PayUtil().payQuery(outTradeNo, channel.getMerchantId(), channel.getAppKey());
                financeService.transactionResults(finance, transactionId, 3, null, new Date());
                applicationEventPublisher.publishEvent(new PaidEvent(outTradeNo, finance.getAmount(), finance.getAttach(), finance.getParams(), finance.getOpUserId()));
            }

            return R.success("success");
        } catch (Exception e) {
            return R.fail(e.getCause() != null ? e.getCause().getMessage() : e.getMessage());
        }
    }

    @PostMapping("refund/submit/{outTradeNo}")
    @ApiOperation(value = "申请退款")
    public R refundSubmit(@PathVariable("outTradeNo") String outTradeNo, @RequestParam(required = false) Integer money, @RequestParam(required = false) String description, @RequestParam Long opUserId) {
        try {
            Finance finance1 = financeService.getByOutTradeNo(outTradeNo, 1);
            if (finance1 == null) {
                return R.fail("交易记录不存在");
            }
            else if (finance1.getStatus() != 3) {
                return R.fail("交易未完成");
            }

            String outRefundNo = String.format("%d", System.currentTimeMillis());
            Finance finance = financeService.beforehandRefund(outTradeNo, money, outRefundNo, description, opUserId);

            Channel channel = channelService.getById(finance.getChannelId());

            PayUtil hwc = new PayUtil();
            hwc.refund(outTradeNo, outRefundNo, finance1.getAmount(), finance.getAmount(), channel.getMerchantId(), channel.getAppKey(), opUserId);
            String transactionId = hwc.refundQuery(outTradeNo, outRefundNo, channel.getMerchantId(), channel.getAppKey());
            financeService.transactionResults(finance, transactionId, 3, null, new Date());
            applicationEventPublisher.publishEvent(new RefundEvent(finance.getOutTradeNo(), finance.getAmount(), finance.getAttach(), finance.getParams(), finance.getOpUserId()));
            return R.success("已退款");
        } catch (Exception e) {
            return R.fail(e.getCause() != null ? e.getCause().getMessage() : e.getMessage());
        }
    }

    @PostMapping("refund/notify")
    @ApiOperation(value = "退款回调通知")
    public void refundNotify(HttpServletRequest request) {

    }

    @GetMapping("refund/query/{outTradeNo}")
    @ApiOperation(value = "查询退款结果")
    public R<Boolean> refundQuery(@PathVariable("outTradeNo") String outTradeNo) {
        try {
            Finance finance = financeService.getByOutTradeNo(outTradeNo, 2);
            if (finance == null) {
                return R.fail("交易记录不存在");
            }

            if (finance.getStatus() != 3) {
                Channel channel = channelService.getById(finance.getChannelId());
                PayUtil hwc = new PayUtil();
                String transactionId = hwc.refundQuery(finance.getOutTradeNo(), finance.getOutRefundNo(), channel.getMerchantId(), channel.getAppKey());
                financeService.transactionResults(finance, transactionId, 3, null, new Date());
                applicationEventPublisher.publishEvent(new RefundEvent(finance.getOutTradeNo(), finance.getAmount(), finance.getAttach(), finance.getParams(), finance.getOpUserId()));
            }

            return R.success("已退款");
        } catch (Exception e) {
            return R.fail(e.getCause() != null ? e.getCause().getMessage() : e.getMessage());
        }
    }
}
