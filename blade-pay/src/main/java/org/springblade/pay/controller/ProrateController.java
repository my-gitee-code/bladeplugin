package org.springblade.pay.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springblade.core.tool.api.R;
import org.springblade.pay.entity.Prorate;
import org.springblade.pay.service.IProrateService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author zhixin
 */
@Slf4j
@Controller
@RequestMapping("prorate")
@Api(tags = "分帐设置接口")
public class ProrateController {

    @Resource
    private IProrateService prorateService;

    @ResponseBody
    @PostMapping()
    @ApiOperation(value = "新增")
    public R add(@RequestBody Prorate prorate) {
        return R.data(prorateService.save(prorate));
    }

    @ResponseBody
    @DeleteMapping("{id}")
    @ApiOperation(value = "删除")
    public R delete(@PathVariable("id") Integer id) {
        LambdaQueryWrapper<Prorate> lqw = new LambdaQueryWrapper<>();
        lqw.eq(Prorate::getId, id);

        Prorate data = new Prorate();
        data.setIsDeleted(1);

        prorateService.update(data, lqw);

        return R.success("success");
    }

    @ResponseBody
    @PutMapping("{id}")
    @ApiOperation(value = "修改")
    public R edit(@PathVariable("id") Integer id, @RequestBody Prorate prorate) {
        LambdaQueryWrapper<Prorate> lqw = new LambdaQueryWrapper<>();
        lqw.eq(Prorate::getId, id);
        prorateService.update(prorate, lqw);
        return R.success("success");
    }

    @GetMapping("{id}")
    @ApiOperation(value = "详情")
    public R detail(@PathVariable("id") Integer id) {
        return R.data(prorateService.getById(id));
    }

    @GetMapping("channel/{channelId}")
    @ApiOperation(value = "通道分帐接收人列表")
    public R query(@PathVariable("channelId") Integer channelId) {
        return R.data(prorateService.getProrateByChannelId(channelId));
    }
}
