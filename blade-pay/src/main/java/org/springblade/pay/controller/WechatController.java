package org.springblade.pay.controller;

import com.alibaba.fastjson.JSONObject;
import com.wechat.pay.java.core.Config;
import com.wechat.pay.java.service.profitsharing.ProfitsharingService;
import com.wechat.pay.java.service.profitsharing.model.*;
import com.wechat.pay.java.service.refund.RefundService;
import com.wechat.pay.java.service.refund.model.AmountReq;
import com.wechat.pay.java.service.refund.model.CreateRequest;
import com.wechat.pay.java.service.refund.model.QueryByOutRefundNoRequest;
import com.wechat.pay.java.service.refund.model.Refund;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springblade.core.tool.api.R;
import org.springblade.pay.endpoint.PayEndpoint;
import org.springblade.pay.entity.Channel;
import org.springblade.pay.entity.Finance;
import org.springblade.pay.entity.Prorate;
import org.springblade.pay.event.PaidEvent;
import org.springblade.pay.event.RefundEvent;
import org.springblade.pay.service.IChannelService;
import org.springblade.pay.service.IFinanceService;
import org.springblade.pay.service.IProrateService;
import org.springblade.pay.util.AesUtil;
import org.springblade.pay.util.WechatUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 微信支付
 * @author zhixin
 */
@Slf4j
@RestController
@RequestMapping("wechat")
@Api(tags = "微信支付接口")
public class WechatController {

    @Value("${blade.http.api}")
    private String host = "https://www.pzk.cn/api";
    @Value("${spring.application.name}")
    private String serverName;
    @Resource
    IChannelService channelService;
    @Resource
    IFinanceService financeService;
    @Resource
    IProrateService prorateService;
    @Resource
    PayEndpoint payEndpoint;
    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    @PostMapping("payment/notify/{channelId}")
    @ApiOperation(value = "支付回调通知")
    public String paymentNotify(@PathVariable("channelId") Integer channelId, @RequestBody String body) {
        log.info("支付回调通知: {}", body);
        try {
            Channel channel = channelService.getById(channelId);
            if (channel != null) {
                JSONObject jsonObject = JSONObject.parseObject(body);
                if (jsonObject.containsKey("event_type") && jsonObject.getString("event_type").equals("TRANSACTION.SUCCESS") &&
                        jsonObject.containsKey("resource_type") && jsonObject.getString("resource_type").equals("encrypt-resource")) {
                    JSONObject resource = jsonObject.getJSONObject("resource");
                    AesUtil aesUtil = new AesUtil(channel.getAppKey().getBytes());
                    String payStr = aesUtil.decryptToString(resource.getString("associated_data").getBytes(), resource.getString("nonce").getBytes(), resource.getString("ciphertext"));
                    log.info("签名验证结果: {}", payStr);

                    JSONObject payJson = JSONObject.parseObject(payStr);
                    String outTradeNo = payJson.getString("out_trade_no");
                    Finance finance = financeService.getByOutTradeNo(outTradeNo, 1);
                    if (finance != null && finance.getStatus() != 3) {
                        if (payJson.getString("trade_state").equals("SUCCESS")) {
                            financeService.transactionResults(finance, payJson.getString("transaction_id"), 3, null, new Date());
                            //发布支付完成事件
                            applicationEventPublisher.publishEvent(new PaidEvent(outTradeNo, finance.getAmount(), finance.getAttach(), finance.getParams(), finance.getOpUserId()));
                        } else {
                            financeService.transactionResults(finance, payJson.getString("transaction_id"), 2, payJson.getString("trade_state_desc"), new Date());
                        }
                    }

                    payEndpoint.send(String.format("%d", finance.getOpUserId()), outTradeNo, 0);
                }
            }
        } catch (Exception e) {
            log.info("支付回调通知异常: {}", e);
        }
        return "{\"code\": \"SUCCESS\",\"message\": \"\"}";
    }

    @PostMapping("refund/submit/{outTradeNo}")
    @ApiOperation(value = "申请退款")
    public R refundSubmit(@PathVariable("outTradeNo") String outTradeNo, @RequestParam(required = false) Integer money, @RequestParam(required = false) String description, @RequestParam Long opUserId) {
        try {
            Finance finance1 = financeService.getByOutTradeNo(outTradeNo, 1);
            if (finance1 == null) {
                return R.fail("交易记录不存在");
            }
            else if (finance1.getStatus() != 3) {
                return R.fail("交易未完成");
            }

            Channel channel = channelService.getById(finance1.getChannelId());
            Config config = WechatUtil.createConfig(channel);

            String outRefundNo = String.valueOf(System.currentTimeMillis());
            Finance finance = financeService.beforehandRefund(outTradeNo, money, outRefundNo, description, opUserId);

            AmountReq amount = new AmountReq();
            amount.setCurrency("CNY");
            amount.setTotal(Long.valueOf(String.format("%d", finance1.getAmount())));
            amount.setRefund(Long.valueOf(String.format("%d", finance.getAmount())));

            CreateRequest request = new CreateRequest();
            request.setOutTradeNo(outTradeNo);
            request.setOutRefundNo(outRefundNo);
            request.setReason(description);
            request.setNotifyUrl(String.format("%s/%s/wechat/refund/notify/%d", host, serverName, channel.getId()));
            request.setAmount(amount);

            RefundService refundService = new RefundService.Builder().config(config).build();
            Refund refund = refundService.create(request);

            finance.setStatus(1);
            financeService.updateById(finance);

            return R.data(refund);
        } catch (Exception e) {
            return R.fail(e.getCause() != null ? e.getCause().getMessage() : e.getMessage());
        }
    }

    @GetMapping("refund/query/{outTradeNo}")
    @ApiOperation(value = "查询退款结果")
    public R<Boolean> refundQuery(@PathVariable("outTradeNo") String outTradeNo) {
        try {
            Finance finance = financeService.getByOutTradeNo(outTradeNo, 2);
            if (finance == null) {
                return R.fail("交易记录不存在");
            }
            else if (finance.getStatus() != 3) {
                return R.success("success");
            }

            Channel channel = channelService.getById(finance.getChannelId());
            Config config = WechatUtil.createConfig(channel);

            QueryByOutRefundNoRequest request = new QueryByOutRefundNoRequest();
            request.setOutRefundNo(finance.getOutRefundNo());

            RefundService refundService = new RefundService.Builder().config(config).build();
            Refund refund = refundService.queryByOutRefundNo(request);

            if (refund.getStatus().equals("SUCCESS")) {
                Date payTime;
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
                try {
                    payTime = sdf.parse(refund.getSuccessTime());
                } catch (Exception e) {
                    payTime = new Date();
                }
                financeService.transactionResults(finance, refund.getTransactionId(), 3, null, payTime);
                applicationEventPublisher.publishEvent(new RefundEvent(finance.getOutTradeNo(), finance.getAmount(), finance.getAttach(), finance.getParams(), finance.getOpUserId()));

                return R.success("success");
            } else {
                financeService.transactionResults(finance, refund.getTransactionId(), 2, null, new Date());
            }

            return R.fail("fail");
        } catch (Exception e) {
            return R.fail(e.getCause() != null ? e.getCause().getMessage() : e.getMessage());
        }
    }

    @PostMapping("refund/notify/{channelId}")
    @ApiOperation(value = "退款回调通知")
    public String refundNotify(@PathVariable("channelId") Integer channelId, @RequestBody String body) {
        log.info("退款回调通知: {}", body);

        try {
            Channel channel = channelService.getById(channelId);
            if (channel != null) {
                JSONObject jsonObject = JSONObject.parseObject(body);
                if (jsonObject.containsKey("event_type") && jsonObject.getString("event_type").equals("REFUND.SUCCESS") &&
                        jsonObject.containsKey("resource_type") && jsonObject.getString("resource_type").equals("encrypt-resource")) {
                    JSONObject resource = jsonObject.getJSONObject("resource");
                    AesUtil aesUtil = new AesUtil(channel.getAppKey().getBytes());
                    String payStr = aesUtil.decryptToString(resource.getString("associated_data").getBytes(), resource.getString("nonce").getBytes(), resource.getString("ciphertext"));
                    log.info("签名验证结果: {}", payStr);

                    JSONObject payJson = JSONObject.parseObject(payStr);
                    String outTradeNo = payJson.getString("out_trade_no");
                    Finance finance = financeService.getByOutTradeNo(outTradeNo, 2);
                    if (finance != null && finance.getStatus() != 3) {
                        if (payJson.getString("refund_status").equals("SUCCESS")) {
                            financeService.transactionResults(finance, payJson.getString("transaction_id"), 3, null, new Date());
                            //发布退款到帐事件
                            applicationEventPublisher.publishEvent(new RefundEvent(outTradeNo, finance.getAmount(), finance.getAttach(), finance.getParams(), finance.getOpUserId()));
                        } else {
                            financeService.transactionResults(finance, payJson.getString("transaction_id"), 2, null, new Date());
                        }
                    }
                }
            }
        } catch (Exception e) {
            log.info("退款回调通知异常: {}", e);
        }

        return "{\"code\": \"SUCCESS\",\"message\": \"\"}";
    }

    @PostMapping("payment/receiver")
    @ApiOperation(value = "添加分账接收方")
    public R addReceiver(@RequestBody Prorate prorate) {
        try {
            Channel channel = channelService.getById(prorate.getChannelId());
            Config config = WechatUtil.createConfig(channel);

            AddReceiverRequest request = new AddReceiverRequest();
            request.setAppid(channel.getAppid());
            request.setType(ReceiverType.PERSONAL_OPENID);
            request.setAccount(prorate.getAccount());
            request.setName(prorate.getName());
            request.setRelationType(ReceiverRelationType.STAFF);

            ProfitsharingService profitsharingService = new ProfitsharingService.Builder().config(config).build();
            AddReceiverResponse response = profitsharingService.addReceiver(request);
            return R.data(response);
        } catch (Exception e) {
            return R.fail(e.getCause() != null ? e.getCause().getMessage() : e.getMessage());
        }
    }

    @PostMapping("payment/prorate")
    @ApiOperation(value = "发起支付-分帐")
    public R paymentProrate(@RequestParam String outOrderNo, @RequestParam Boolean unfreezeUnsplit) {
        try {
            Finance finance = financeService.getByOutTradeNo(outOrderNo, 1);
            if (finance == null) {
                return R.fail("交易记录不存在");
            }
            else if (finance.getStatus() != 3) {
                return R.fail("交易未完成");
            }

            Channel channel = channelService.getById(finance.getChannelId());
            Config config = WechatUtil.createConfig(channel);

            List<CreateOrderReceiver> receivers = new ArrayList();
            List<Prorate> prorates = prorateService.getProrateByChannelId(channel.getId());
            prorates.forEach(item -> {
                if (item.getStatus() == 1 && item.getBrokerage() > 0) {
                    Double amount = finance.getAmount() * item.getBrokerage();
                    CreateOrderReceiver receiver = new CreateOrderReceiver();
                    receiver.setType(item.getType());
                    receiver.setAccount(item.getAccount());
                    receiver.setName(item.getName());
                    receiver.setAmount(amount.longValue());
                    receiver.setDescription(item.getDescription());
                    receivers.add(receiver);
                }
            });

            if (receivers.size() == 0) {
                return R.fail("未设置分账接收方");
            }

            CreateOrderRequest request = new CreateOrderRequest();
            request.setAppid(channel.getAppid());
            request.setOutOrderNo(outOrderNo);
            request.setTransactionId(finance.getTransactionId());
            request.setUnfreezeUnsplit(unfreezeUnsplit);
            request.setReceivers(receivers);

            ProfitsharingService profitsharingService = new ProfitsharingService.Builder().config(config).build();
            OrdersEntity entity = profitsharingService.createOrder(request);
            return R.data(entity);
        } catch (Exception e) {
            return R.fail(e.getCause() != null ? e.getCause().getMessage() : e.getMessage());
        }
    }
}
