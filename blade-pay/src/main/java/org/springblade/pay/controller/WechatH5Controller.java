package org.springblade.pay.controller;

import com.wechat.pay.java.core.Config;
import com.wechat.pay.java.service.payments.h5.H5Service;
import com.wechat.pay.java.service.payments.h5.model.*;
import com.wechat.pay.java.service.payments.model.Transaction;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springblade.core.tool.api.R;
import org.springblade.pay.entity.Channel;
import org.springblade.pay.entity.Finance;
import org.springblade.pay.event.PaidEvent;
import org.springblade.pay.service.IChannelService;
import org.springblade.pay.service.IFinanceService;
import org.springblade.pay.util.WechatUtil;
import org.springblade.pay.vo.PaymentVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 微信H5支付
 * @author zhixin
 */
@Slf4j
@RestController
@RequestMapping("wechat/h5")
@Api(tags = "微信H5支付接口")
public class WechatH5Controller {

    @Value("${blade.http.api}")
    private String host = "https://www.pzk.cn/api";
    @Value("${spring.application.name}")
    private String serverName;
    @Resource
    IFinanceService financeService;
    @Resource
    IChannelService channelService;
    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    @PostMapping("payment/submit")
    @ApiOperation(value = "发起支付")
    public R paymentSubmit(@RequestHeader HttpHeaders headers, @RequestBody PaymentVO paymentVO) {
        try {
            Channel channel = channelService.getById(paymentVO.getChannelId());
            if (channel == null) {
                return R.fail("未设置支付通道");
            }

            Finance finance = financeService.beforehandPay(paymentVO, "微信公众号", channel.getBrokerage(), paymentVO.getOpUserId());
            Config config = WechatUtil.createConfig(channel);

            Amount amount = new Amount();
            amount.setCurrency("CNY");
            amount.setTotal(paymentVO.getAmount());

            PrepayRequest request = new PrepayRequest();
            request.setAmount(amount);
            request.setAppid(channel.getAppid());
            request.setMchid(channel.getMerchantId());

            SettleInfo settleInfo = new SettleInfo();
            settleInfo.setProfitSharing(paymentVO.getProfitSharing());
            request.setSettleInfo(settleInfo);

            request.setAttach(paymentVO.getAttach());
            request.setDescription(paymentVO.getDescription());
            request.setNotifyUrl(String.format("%s/%s/wechat/payment/notify/%d", host, serverName, channel.getId()));
            request.setOutTradeNo(paymentVO.getOutTradeNo());

            H5Service h5Service = new H5Service.Builder().config(config).build();
            PrepayResponse response = h5Service.prepay(request);

            finance.setStatus(1);
            financeService.updateById(finance);

            return R.data(response);
        } catch (Exception e) {
            return R.fail(e.getCause() != null ? e.getCause().getMessage() : e.getMessage());
        }
    }

    @GetMapping("payment/query/{outTradeNo}")
    @ApiOperation(value = "查询支付结果")
    public R<Boolean> paymentQuery(@PathVariable("outTradeNo") String outTradeNo) {
        try {
            Finance finance = financeService.getByOutTradeNo(outTradeNo, 1);
            if (finance == null) {
                return R.fail("交易记录不存在");
            }
            else if (finance.getStatus() == 3) {
                return R.success("success");
            }

            Channel channel = channelService.getById(finance.getChannelId());
            Config config = WechatUtil.createConfig(channel);

            QueryOrderByOutTradeNoRequest request = new QueryOrderByOutTradeNoRequest();
            request.setOutTradeNo(outTradeNo);
            request.setMchid(channel.getMerchantId());

            H5Service h5Service = new H5Service.Builder().config(config).build();
            Transaction transaction = h5Service.queryOrderByOutTradeNo(request);
            if (transaction.getTradeState() == Transaction.TradeStateEnum.SUCCESS) {
                Date payTime;
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
                try {
                    payTime = sdf.parse(transaction.getSuccessTime());
                } catch (Exception e) {
                    payTime = new Date();
                }
                financeService.transactionResults(finance, transaction.getTransactionId(), 3, null, payTime);
                applicationEventPublisher.publishEvent(new PaidEvent(outTradeNo, finance.getAmount(), finance.getAttach(), finance.getParams(), finance.getOpUserId()));
                return R.success("success");
            }

            return R.fail("fail");
        } catch (Exception e) {
            return R.fail(e.getCause() != null ? e.getCause().getMessage() : e.getMessage());
        }
    }

}
