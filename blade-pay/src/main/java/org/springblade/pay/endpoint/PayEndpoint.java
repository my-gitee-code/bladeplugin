package org.springblade.pay.endpoint;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArraySet;

@Slf4j
@Component
@ServerEndpoint("/wss/pay/{userId}")
public class PayEndpoint {
    /**
     * 与某个客户端的连接会话，需要通过它来给客户端发送数据
     */
    private Session session;
    private String userId;
    private static CopyOnWriteArraySet<PayEndpoint> webSockets = new CopyOnWriteArraySet<>();
    /**
     * 用来存在线连接用户信息
     */
    private static ConcurrentHashMap<String, Session> sessionPool = new ConcurrentHashMap<String, Session>();

    /**
     * 链接成功调用的方法
     */
    @OnOpen
    public void onOpen(Session session, @PathParam(value="userId")String userId) {
        try {
            this.session = session;
            this.userId = userId;
            webSockets.add(this);
            sessionPool.put(userId, session);
//			log.info("【websocket消息】有新的连接，总数为:"+webSockets.size());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 链接关闭调用的方法
     */
    @OnClose
    public void onClose() {
        try {
            webSockets.remove(this);
            sessionPool.remove(this.userId);
//			log.info("【websocket消息】连接断开，总数为:"+webSockets.size());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 收到客户端消息后调用的方法
     * @param message
     */
    @OnMessage
    public void onMessage(String message) {
        log.info("【websocket消息】收到客户端消息:"+message);
    }

    /** 发送错误时的处理
     * @param session
     * @param error
     */
    @OnError
    public void onError(Session session, Throwable error) {

        log.error("用户错误,原因:"+error.getMessage());
        error.printStackTrace();
    }


    public void send(Object obj) {
        send(JSON.toJSONString(obj));
    }

    /**
     * 此为广播消息
     * @param message
     */
    public void send(String message) {
        log.info("【websocket消息】广播消息:"+message);
        for(PayEndpoint webSocket : webSockets) {
            try {
                if(webSocket.session.isOpen()) {
                    webSocket.session.getAsyncRemote().sendText(message);
                }
            } catch (Exception e) {
                log.error("消息发送失败", e);
            }
        }
    }

    public void send(String userId, Object obj) {
        send(userId, JSON.toJSONString(obj));
    }

    /**
     *
     * @param userId
     * @param outTradeNo 订单号
     * @param code 状态码:0=成功
     */
    public void send(String userId, String outTradeNo, Integer code) {
        String message = String.format("{\"userId\":\"%d\",\"outTradeNo\":\"%s\",\"code\":\"%d\"}", userId, outTradeNo, code);
        send(userId, message);
    }

    /**
     * 此为单点消息
     * @param userId
     * @param message
     */
    public void send(String userId, String message) {
        Session session = sessionPool.get(userId);
        if (session != null&&session.isOpen()) {
            try {
                log.info("【websocket消息】 单点消息:"+message);
                session.getAsyncRemote().sendText(message);
            } catch (Exception e) {
                log.error("消息发送失败", e);
            }
        }
    }

    public void send(String[] userIds, Object obj) {
        send(userIds, JSON.toJSONString(obj));
    }

    /**
     * 此为单点消息(多人)
     * @param userIds
     * @param message
     */
    public void send(String[] userIds, String message) {
        for(String userId:userIds) {
            Session session = sessionPool.get(userId);
            if (session != null&&session.isOpen()) {
                try {
                    log.info("【websocket消息】 单点消息:"+message);
                    session.getAsyncRemote().sendText(message);
                } catch (Exception e) {
                    log.error("消息发送失败", e);
                }
            }
        }

    }
}
