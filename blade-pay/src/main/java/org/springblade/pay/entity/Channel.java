package org.springblade.pay.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 支付通道设置
 * @author zhixin
 */
@Data
@TableName("blade_pay_channel")
@ApiModel(value = "Channel", description = "支付通道设置")
public class Channel {

    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Integer id;

    @ApiModelProperty(value = "通道名称")
    private String name;

    @ApiModelProperty(value = "支付渠道名称:微信小程序,微信公众号,汇旺财,支付宝")
    private String title;

    @ApiModelProperty(value = "微信appId")
    private String appid;

    @ApiModelProperty(value = "微信支付商户号,汇旺财门店编号")
    private String merchantId;

    @ApiModelProperty(value = "私匙文件路径")
    private String privateKeyPath;

    @ApiModelProperty(value = "秘匙")
    private String merchantSerial;

    @ApiModelProperty(value = "微信APIv3密钥,汇旺财MD5密钥")
    private String appKey;

    @ApiModelProperty(value = "支付手续费")
    private Double brokerage;

    @ApiModelProperty(value = "接口(控制器)路径")
    private String controller;

    @ApiModelProperty(value = "状态:0=未审核,1=已启用,2=已停用", example = "0")
    private Integer status;

    @ApiModelProperty(value = "是否已删除", example = "0")
    private Integer isDeleted;

    @ApiModelProperty(value = "新增或修改时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;
}
