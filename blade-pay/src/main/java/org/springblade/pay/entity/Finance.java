package org.springblade.pay.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 交易明细
 * @author zhixin
 */
@Data
@ColumnWidth(25)
@HeadRowHeight(20)
@ContentRowHeight(18)
@TableName("blade_pay_finance")
@ApiModel(value = "Finance", description = "交易明细")
public class Finance {

    @ColumnWidth(15)
    @ExcelProperty("系统编号")
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    @ColumnWidth(15)
    @ExcelProperty("订单号")
    @ApiModelProperty(value = "订单号")
    private String outTradeNo;

    @ColumnWidth(15)
    @ExcelProperty("退款单号")
    @ApiModelProperty(value = "退款单号")
    private String outRefundNo;

    @ColumnWidth(15)
    @ExcelProperty("交易金额")
    @ApiModelProperty(value = "交易金额,单位:分", example = "1")
    private Integer amount;

    @ColumnWidth(15)
    @ExcelProperty("交易手续费")
    @ApiModelProperty(value = "交易手续费")
    private Double brokerage;

    @ColumnWidth(15)
    @ExcelProperty("附加信息")
    @ApiModelProperty(value = "附加信息")
    private String attach;

    @ColumnWidth(15)
    @ExcelProperty("交易类型")
    @ApiModelProperty(value = "交易类型:1=支付;2=退款", example = "0")
    private Integer transactionType;

    @ColumnWidth(15)
    @ExcelProperty("支付方式")
    @ApiModelProperty(value = "支付方式:微信公众号")
    private String payMode;

    @ColumnWidth(15)
    @ExcelProperty("三方交易单号")
    @ApiModelProperty(value = "三方交易单号")
    private String transactionId;

    @ColumnWidth(15)
    @ExcelProperty("状态")
    @ApiModelProperty(value = "状态:0=未处理;1=待处理;2=失败;3=成功;4=已转入退款", example = "0")
    private Integer status;

    @ColumnWidth(15)
    @ExcelProperty("扩展参数")
    @ApiModelProperty(value = "扩展参数")
    private String params;

    @ColumnWidth(15)
    @ExcelProperty("备注")
    @ApiModelProperty(value = "备注")
    private String description;

    @ColumnWidth(15)
    @ExcelProperty("错误")
    @ApiModelProperty(value = "错误")
    private String error;

    @ColumnWidth(15)
    @ExcelProperty("支付通道")
    @ApiModelProperty(value = "支付通道编号")
    private Integer channelId;

    @ColumnWidth(15)
    @ExcelProperty("操作员")
    @ApiModelProperty(value = "操作员")
    private Long opUserId;

    @ColumnWidth(15)
    @ExcelProperty("预处理时间")
    @ApiModelProperty(value = "预处理时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @ColumnWidth(15)
    @ExcelProperty("交易时间")
    @ApiModelProperty(value = "交易时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
}
