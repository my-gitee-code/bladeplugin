package org.springblade.pay.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 分账接收方设置
 * @author zhixin
 */
@Data
@TableName("blade_pay_prorate")
@ApiModel(value = "Prorate", description = "分账接收方设置")
public class Prorate {

    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Integer id;

    @ApiModelProperty(value = "支付通道编号", required = true)
    private Integer channelId;

    @ApiModelProperty(value = "分账接收方类型:MERCHANT_ID,PERSONAL_OPENID", required = true)
    private String type;

    @ApiModelProperty(value = "分账接收方账号:MERCHANT_ID=商户号,PERSONAL_OPENID=个人openid", required = true)
    private String account;

    @ApiModelProperty(value = "分账个人接收方姓名", required = true)
    private String name;

    @ApiModelProperty(value = "与分账方的关系类型:STORE：门店;STAFF：员工;STORE_OWNER：店主;PARTNER：合作伙伴;HEADQUARTER：总部;BRAND：品牌方;DISTRIBUTOR：分销商;USER：用户;SUPPLIER： 供应商;CUSTOM：自定义", required = true)
    private String relationType;

    @ApiModelProperty(value = "自定义的分账关系:如代理商", required = false)
    private String customRelation;

    @ApiModelProperty(value = "分帐比例:0.0038", example = "0")
    private Double brokerage;

    @ApiModelProperty(value = "状态:0=禁用;1=启用", example = "0")
    private Integer status;

    @ApiModelProperty(value = "备注")
    private String description;

    @ApiModelProperty(value = "是否已删除", example = "0")
    private Integer isDeleted;

    @ApiModelProperty(value = "新增或修改时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;
}
