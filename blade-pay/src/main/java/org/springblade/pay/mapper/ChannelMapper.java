package org.springblade.pay.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springblade.pay.entity.Channel;

public interface ChannelMapper extends BaseMapper<Channel> {
}
