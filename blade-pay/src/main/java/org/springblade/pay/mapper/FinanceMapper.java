package org.springblade.pay.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;
import org.springblade.pay.entity.Finance;
import org.springblade.pay.vo.FinanceSumVO;

import java.util.Date;

/**
 * @author zhixin
 */
public interface FinanceMapper extends BaseMapper<Finance> {

    /**
     * 数据汇总
     * @return
     */
    @Select("select count(out_trade_no) as total,sum(amount) as amount, sum(brokerage) as brokerage from pay_finance where transaction_type=#{type} and status in (3,4) and create_time>='#{startTime}' and create_time<='#{endTime}' group by out_trade_no")
    FinanceSumVO summary(Integer type, Date startTime, Date endTime);
}
