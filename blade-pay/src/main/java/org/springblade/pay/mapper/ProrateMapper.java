package org.springblade.pay.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springblade.pay.entity.Prorate;

public interface ProrateMapper extends BaseMapper<Prorate> {
}
