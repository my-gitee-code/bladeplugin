package org.springblade.pay.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.springblade.pay.entity.Channel;

public interface IChannelService extends IService<Channel> {
}
