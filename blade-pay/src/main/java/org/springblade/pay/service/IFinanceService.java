package org.springblade.pay.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.springblade.pay.entity.Finance;
import org.springblade.pay.vo.PaymentVO;

import java.util.Date;

public interface IFinanceService extends IService<Finance> {

    /**
     *
     * @param outTradeNo
     * @param transactionType 交易类型:1=支付;2=退款
     * @return
     */
    Finance getByOutTradeNo(String outTradeNo, Integer transactionType);

    /**
     * 预支付
     * @param paymentVO
     * @param payMode 支付方式:微信公众号
     * @param brokerage 支付手续费
     * @param opUserId 操作员
     * @return
     */
    Finance beforehandPay(PaymentVO paymentVO, String payMode, Double brokerage, Long opUserId);

    /**
     * 预退款
     * @param outTradeNo
     * @param money
     * @param outRefundNo
     * @param description
     * @param opUserId
     * @return
     */
    Finance beforehandRefund(String outTradeNo, Integer money, String outRefundNo, String description, Long opUserId);

    /**
     * 更新交易结果
     * @param finance
     * @param transactionId
     * @param status
     * @param error 失败原因
     * @return
     */
    Boolean transactionResults(Finance finance, String transactionId, Integer status, String error, Date payTime);
}
