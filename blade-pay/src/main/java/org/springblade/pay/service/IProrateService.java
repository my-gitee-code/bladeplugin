package org.springblade.pay.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.springblade.pay.entity.Prorate;

import java.util.List;

public interface IProrateService extends IService<Prorate> {

    /**
     * 通道分帐接收人列表
     * @param channelId
     * @return
     */
    List<Prorate> getProrateByChannelId(Integer channelId);
}
