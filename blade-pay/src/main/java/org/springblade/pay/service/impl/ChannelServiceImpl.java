package org.springblade.pay.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springblade.pay.entity.Channel;
import org.springblade.pay.mapper.ChannelMapper;
import org.springblade.pay.service.IChannelService;
import org.springframework.stereotype.Service;

@Service
public class ChannelServiceImpl extends ServiceImpl<ChannelMapper, Channel> implements IChannelService {
}
