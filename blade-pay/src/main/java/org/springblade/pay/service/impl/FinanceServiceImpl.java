package org.springblade.pay.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.SneakyThrows;
import org.springblade.pay.entity.Finance;
import org.springblade.pay.mapper.FinanceMapper;
import org.springblade.pay.service.IFinanceService;
import org.springblade.pay.vo.PaymentVO;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class FinanceServiceImpl extends ServiceImpl<FinanceMapper, Finance> implements IFinanceService {

    @Override
    public Finance getByOutTradeNo(String outTradeNo, Integer transactionType) {
        LambdaQueryWrapper<Finance> lqw = new LambdaQueryWrapper<>();
        lqw.eq(Finance::getOutTradeNo, outTradeNo);
        lqw.eq(Finance::getTransactionType, transactionType);
        lqw.orderByDesc(Finance::getCreateTime);
        return count(lqw) == 0L ? null : list(lqw).get(0);
    }

    @Override
    @SneakyThrows
    public Finance beforehandPay(PaymentVO paymentVO, String payMode, Double brokerage, Long opUserId) {
        Finance finance = new Finance();
        finance.setOutTradeNo(paymentVO.getOutTradeNo());
        finance.setAmount(paymentVO.getAmount());
        finance.setBrokerage(brokerage * paymentVO.getAmount());
        finance.setAttach(paymentVO.getAttach());
        finance.setTransactionType(1);
        finance.setPayMode(payMode);
        finance.setStatus(0);
        finance.setParams(paymentVO.getParams());
        finance.setDescription(paymentVO.getDescription());
        finance.setChannelId(paymentVO.getChannelId());
        finance.setOpUserId(opUserId);
        finance.setCreateTime(new Date());
        save(finance);
        return finance;
    }

    @Override
    @SneakyThrows
    public Finance beforehandRefund(String outTradeNo, Integer money, String outRefundNo, String description, Long opUserId) {
        LambdaQueryWrapper<Finance> lqw = new LambdaQueryWrapper<>();
        lqw.eq(Finance::getOutTradeNo, outTradeNo);
        lqw.eq(Finance::getTransactionType, 1);
        lqw.eq(Finance::getStatus, 3);
        lqw.orderByDesc(Finance::getCreateTime);
        if (count(lqw) == 0L) {
            throw new Exception("交易记录不存在");
        }

        Finance payFinance = list(lqw).get(0);

        if (money == null || money == 0) {
            money = payFinance.getAmount();
        }

        if (money > payFinance.getAmount()) {
            throw new Exception("退款金额不得大于订单总金额");
        }

        Finance refundFinance = new Finance();
        refundFinance.setOutTradeNo(payFinance.getOutTradeNo());
        refundFinance.setOutRefundNo(outRefundNo);
        refundFinance.setAmount(money);
        refundFinance.setAttach(payFinance.getAttach());
        refundFinance.setTransactionType(2);
        refundFinance.setPayMode(payFinance.getPayMode());
        refundFinance.setStatus(0);
        refundFinance.setParams(payFinance.getParams());
        refundFinance.setDescription(description);
        refundFinance.setChannelId(payFinance.getChannelId());
        refundFinance.setOpUserId(opUserId);
        refundFinance.setCreateTime(new Date());

        save(refundFinance);

        payFinance.setStatus(4);
        updateById(payFinance);

        return refundFinance;
    }

    @Override
    public Boolean transactionResults(Finance finance, String transactionId, Integer status, String error, Date payTime) {
        finance.setTransactionId(transactionId);
        finance.setStatus(status);
        finance.setError(error);
        finance.setUpdateTime(payTime == null ? new Date() : payTime);
        return updateById(finance);
    }
}
