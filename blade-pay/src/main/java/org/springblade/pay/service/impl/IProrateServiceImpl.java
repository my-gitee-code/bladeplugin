package org.springblade.pay.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springblade.pay.entity.Prorate;
import org.springblade.pay.mapper.ProrateMapper;
import org.springblade.pay.service.IProrateService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IProrateServiceImpl extends ServiceImpl<ProrateMapper, Prorate> implements IProrateService {

    @Override
    public List<Prorate> getProrateByChannelId(Integer channelId) {
        LambdaQueryWrapper<Prorate> lqw = new LambdaQueryWrapper<>();
        lqw.eq(Prorate::getChannelId, channelId);
        lqw.eq(Prorate::getIsDeleted, 0);
        return list(lqw);
    }

}
