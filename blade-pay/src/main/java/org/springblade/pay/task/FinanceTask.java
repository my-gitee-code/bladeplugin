package org.springblade.pay.task;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.springblade.core.mp.support.Query;
import org.springblade.pay.builder.PayBuilder;
import org.springblade.pay.entity.Channel;
import org.springblade.pay.entity.Finance;
import org.springblade.pay.service.IChannelService;
import org.springblade.pay.service.IFinanceService;
import org.springblade.pay.builder.template.PayTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author zhixin
 */
@Slf4j
@Component
public class FinanceTask {

    @Resource
    private IChannelService channelService;
    @Resource
    private IFinanceService financeService;

    /**
     * 每分钟检查一次: 查询退款结果
     */
    @Scheduled(fixedDelay = 60000)
    public void queryRefundResults() {
        LambdaQueryWrapper<Finance> lqw = new LambdaQueryWrapper<>();
        lqw.eq(Finance::getTransactionType, 2);
        lqw.eq(Finance::getStatus, 1);
        lqw.orderByDesc(Finance::getCreateTime);

        if (financeService.count(lqw) == 0L) {
            return;
        }

        Query query = new Query();
        query.setCurrent(1);
        query.setSize(10);

        Page<Finance> financePage = new Page<>();
        financePage = financeService.page(financePage, lqw);

        financePage.getRecords().forEach(finance -> {
            Channel channel = channelService.getById(finance.getChannelId());
            PayTemplate payTemplate = new PayBuilder().template(channel);
            try {
                Finance refundFinance = payTemplate.refundQuery(channel, finance);
                if (refundFinance != null) {
                    LambdaQueryWrapper<Finance> lqw2 = new LambdaQueryWrapper<>();
                    lqw2.eq(Finance::getId, finance.getId());
                    lqw2.eq(Finance::getStatus, 1);
                    financeService.update(refundFinance, lqw2);
                }
            } catch (Exception e) {
                log.info("查询退款结果异常: {}", e);
            }
        });
    }

}
