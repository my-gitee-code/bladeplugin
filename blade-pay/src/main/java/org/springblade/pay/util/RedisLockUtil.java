package org.springblade.pay.util;

import org.springblade.core.redis.cache.BladeRedis;
import org.springblade.core.tool.utils.SpringUtil;

/**
 * @author zhixin
 * redis锁处理
 */
public class RedisLockUtil {

    private String prefix = "blade:pay:lock:";
    private String key = "";

    /**
     * 加锁,获取锁
     * @param key
     * @param desc
     */
    public String lock(String key, String desc) {
        return lock(key, desc, 60);
    }

    /**
     * 加锁,获取锁
     * @param key
     * @param desc
     * @param timeout 超时时间，单位：秒
     */
    public synchronized String lock(String key, String desc, Integer timeout) {
        this.key = prefix.concat(key);
        BladeRedis bladeRedis = SpringUtil.getBean("bladeRedis");

        Long time1 = System.currentTimeMillis();
        while (bladeRedis.exists(this.key)) {
            Long time2 = System.currentTimeMillis();
            if (time1 + timeout * 1000 <= time2) {
                return bladeRedis.get(this.key);
            }

            try {
                Thread.sleep(100);
            } catch (Exception e) {

            }
        }

//		bladeRedis.set(this.key, value);
        //为防止死锁，5分钟后失效
        bladeRedis.setEx(this.key, desc, 300L);

        return null;
    }

    /**
     * 释放锁
     */
    public synchronized void unlock() {
        BladeRedis bladeRedis = SpringUtil.getBean("bladeRedis");
        if (!this.key.isEmpty() && bladeRedis.exists(this.key)) {
            bladeRedis.del(this.key);
        }
    }
}
