package org.springblade.pay.util;

import com.wechat.pay.java.core.Config;
import com.wechat.pay.java.core.RSAAutoCertificateConfig;
import org.springblade.pay.entity.Channel;

/**
 * @author zhixin
 */
public class WechatUtil {

    /**
     * 微信支付设置
     * @param channel
     * @return
     * @throws Exception
     */
    public static Config createConfig(Channel channel) throws Exception {
        if (channel == null) {
            throw new Exception("未设置支付通道");
        }
        if (channel.getMerchantId() == null || channel.getMerchantId().isEmpty()) {
            throw new Exception("微信支付商户号不能为空");
        }
        if (channel.getPrivateKeyPath() == null || channel.getPrivateKeyPath().isEmpty()) {
            throw new Exception("微信私匙不能为空");
        }
        if (channel.getMerchantSerial() == null || channel.getMerchantSerial().isEmpty()) {
            throw new Exception("微信秘匙不能为空");
        }
        if (channel.getAppKey() == null || channel.getAppKey().isEmpty()) {
            throw new Exception("微信APIv3密钥不能为空");
        }

        Config config = new RSAAutoCertificateConfig.Builder()
                .merchantId(channel.getMerchantId())
                .privateKeyFromPath(channel.getPrivateKeyPath())
                .merchantSerialNumber(channel.getMerchantSerial())
                .apiV3Key(channel.getAppKey())
                .build();
        return config;
    }
}
