package org.springblade.pay.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zhixin
 */
@Data
@ApiModel(value = "FinanceSumVO", description = "交易数据汇总")
public class FinanceSumVO {

    @ApiModelProperty(value = "交易金额,单位:分", example = "1")
    private Integer amount;

    @ApiModelProperty(value = "交易手续费")
    private Double brokerage;

    @ApiModelProperty(value = "交易笔数")
    private Integer total;
}
