CREATE TABLE `blade_tags_data` (
  `id` int NOT NULL AUTO_INCREMENT,
  `type_id` int NOT NULL COMMENT '标签类型编号',
  `tags_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '标签',
  PRIMARY KEY (`id`),
  KEY `tb_tags_data_type_id_IDX` (`type_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='标签';

CREATE TABLE `blade_tags_type` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '类型名称',
  `expression` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '表达式',
  `echarts_type` varchar(10) COLLATE utf8mb4_bin NOT NULL DEFAULT 'bar' COMMENT '图表类型',
  `unit` varchar(10) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '单位',
  `query` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '自定义查询预计',
  `dict_key` varchar(100) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '字典键名',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='标签类型';

INSERT INTO blade_tags_type
(id, name, expression, echarts_type, unit, query)
VALUES(1, '预定时间', 'dateTimeUtils.getHour(create_time);', 'bar', NULL, NULL);
INSERT INTO share_teahouse.blade_tags_type
(id, name, expression, echarts_type, unit, query)
VALUES(2, '星期', 'dateTimeUtils.getWeek(start_time);', 'bar', NULL, NULL);
INSERT INTO share_teahouse.blade_tags_type
(id, name, expression, echarts_type, unit, query)
VALUES(3, '月份', 'dateTimeUtils.getMonth(start_time);', 'bar', NULL, NULL);
INSERT INTO share_teahouse.blade_tags_type
(id, name, expression, echarts_type, unit, query)
VALUES(4, '包间类型', 'type_name', 'bar', NULL, NULL);
INSERT INTO share_teahouse.blade_tags_type
(id, name, expression, echarts_type, unit, query)
VALUES(5, '性别', NULL, 'bar', NULL, NULL);
INSERT INTO share_teahouse.blade_tags_type
(id, name, expression, echarts_type, unit, query)
VALUES(6, '来源', 'scene', 'bar', NULL, NULL);
INSERT INTO share_teahouse.blade_tags_type
(id, name, expression, echarts_type, unit, query)
VALUES(7, '消费金额', NULL, 'bar', NULL, NULL);
INSERT INTO share_teahouse.blade_tags_type
(id, name, expression, echarts_type, unit, query)
VALUES(8, '手机类型', NULL, 'bar', NULL, NULL);
INSERT INTO share_teahouse.blade_tags_type
(id, name, expression, echarts_type, unit, query)
VALUES(9, '月续费率', NULL, 'bar', NULL, 'select month,max(total),min(total),round((min(total)/max(total))*100,0) as ratio from (select month,is_renew,count(*) as total from (select month(start_time) as month,is_renew,user_id,room_id,shop_id,price,deal_price from tb_shop_reserve_finish) as a group by month,is_renew order by month asc,is_renew asc) as b group by month order by month asc');
INSERT INTO share_teahouse.blade_tags_type
(id, name, expression, echarts_type, unit, query)
VALUES(10, '年龄', NULL, 'bar', NULL, NULL);
INSERT INTO share_teahouse.blade_tags_type
(id, name, expression, echarts_type, unit, query)
VALUES(11, '消费时间', 'dateTimeUtils.getTimeOccupyHour(start_time, end_time);', 'bar', NULL, NULL);
INSERT INTO share_teahouse.blade_tags_type
(id, name, expression, echarts_type, unit, query)
VALUES(12, '提前预定时间', 'dateTimeUtils.getTimeDiffHour(create_time, start_time);', 'bar', NULL, NULL);
INSERT INTO share_teahouse.blade_tags_type
(id, name, expression, echarts_type, unit, query)
VALUES(13, '月退费率', NULL, 'bar', NULL, NULL);

CREATE TABLE `blade_tags_user` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `user_id` bigint NOT NULL COMMENT '用户编号',
  `tags_id` int NOT NULL COMMENT '标签编号',
  `dest_id` bigint NOT NULL COMMENT '主表编号',
  `year` int NOT NULL COMMENT '年份',
  `month` int NOT NULL COMMENT '月份',
  `week` int DEFAULT NULL COMMENT '星期',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='用户画像';

