package org.springblade.portrait;

import org.springblade.core.launch.BladeApplication;
import org.springframework.cloud.client.SpringCloudApplication;

/**
 * @author zhixin
 */
@SpringCloudApplication
public class PortraitApplication {

	public static void main(String[] args) {
		BladeApplication.run("blade-portrait", PortraitApplication.class, args);
	}

}
