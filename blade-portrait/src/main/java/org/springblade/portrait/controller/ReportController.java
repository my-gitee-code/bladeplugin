package org.springblade.portrait.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springblade.core.tool.api.R;
import org.springblade.portrait.entity.TagsType;
import org.springblade.portrait.mapper.TagsUserMapper;
import org.springblade.portrait.service.ITagsTypeService;
import org.springblade.portrait.service.ITagsUserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("report")
@Api(tags = "报表")
public class ReportController {
	
	@Resource
	private ITagsTypeService tagsTypeService;
	@Resource
	private ITagsUserService tagsUserService;
	@Resource
	private TagsUserMapper tagsUserMapper;
	
    @ResponseBody
    @GetMapping("data/{id}")
    @ApiOperation(value = "新增或修改")
    public R<JSONObject> getReportData(@PathVariable("id") Integer id, @RequestParam(required = false) List<Long> userIds) {
    	Map result = tagsUserService.getReportData(id, userIds);
    	JSONObject jsonObject = new JSONObject();
    	jsonObject.put("data", result);
    	jsonObject.put("total", userIds == null ? 1 : userIds.size() + 1);
        return R.data(jsonObject);
    }

    @ResponseBody
    @GetMapping("user/{userId}")
    @ApiOperation(value = "获取用户数据")
    public R getUserTags(@PathVariable("userId") Long userId) {
    	Map<String, JSONArray> result = new HashMap<>();
    	
    	List<Map<String, Object>> userTags = tagsUserMapper.getUserTags(userId);
    	userTags.forEach(map -> {
    		String name = map.get("name").toString();
    		if (!result.containsKey(name)) {
    			result.put(name, new JSONArray());
    		}
    		
    		JSONObject item = new JSONObject();
    		item.put("name", map.get("tags_name").toString());
    		item.put("value", map.get("total"));
    		result.get(name).add(item);
    	});
    	
    	return R.data(result);
    }
    
    @GetMapping("index")
    @ApiOperation(value = "分页查询")
    public String page(Model model) {
    	List<TagsType> tags = tagsTypeService.list();
    	
    	model.addAttribute("title", "用户画像");
    	model.addAttribute("tags", tags);
    	return "report/index";
    }

}
