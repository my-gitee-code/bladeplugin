package org.springblade.portrait.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@TableName(value ="blade_tags_data")
@ApiModel(value="TagsData", description="标签值")
public class TagsData {

	@TableId(value = "id", type = IdType.AUTO)
	@ApiModelProperty(value = "编号")
	private Integer id;

	@ApiModelProperty(value = "标签类型编号")
	private Integer typeId;

	@ApiModelProperty(value = "标签")
	private String tagsName;

}
