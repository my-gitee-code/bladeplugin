package org.springblade.portrait.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@TableName(value ="blade_tags_type")
@ApiModel(value="TagsType", description="标签类型")
public class TagsType {

	@TableId(value = "id", type = IdType.AUTO)
	@ApiModelProperty(value = "编号")
	private Integer id;

	@ApiModelProperty(value = "标签名称")
	private String name;

	@ApiModelProperty(value = "图表类型")
	private String echartsType;

	@ApiModelProperty(value = "表达式")
	private String expression;

	@ApiModelProperty(value = "自定义查询语句")
	private String query;

	@ApiModelProperty(value = "单位")
	private String unit;

}
