package org.springblade.portrait.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@TableName(value ="blade_tags_user")
@ApiModel(value="TagsUser", description="用户画像")
public class TagsUser {

	@TableId(value = "id", type = IdType.AUTO)
	@ApiModelProperty(value = "编号")
	private Long id;
	
	@ApiModelProperty(value = "用户编号")
	private Long userId;
	
	@ApiModelProperty(value = "标签编号")
	private Integer tagsId;
	
	@ApiModelProperty(value = "主表编号")
	private Long destId;

	@ApiModelProperty(value = "年份")
	private Integer year;

	@ApiModelProperty(value = "月份")
	private Integer month;

	@ApiModelProperty(value = "星期")
	private Integer week;

}
