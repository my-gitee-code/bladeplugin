package org.springblade.portrait.mapper;

import org.springblade.portrait.entity.TagsData;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface TagsDataMapper extends BaseMapper<TagsData> {

}
