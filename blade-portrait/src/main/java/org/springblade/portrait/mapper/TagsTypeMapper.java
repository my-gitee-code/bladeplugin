package org.springblade.portrait.mapper;

import org.springblade.portrait.entity.TagsType;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface TagsTypeMapper extends BaseMapper<TagsType> {

}
