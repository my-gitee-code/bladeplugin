package org.springblade.portrait.mapper;

import java.util.List;
import java.util.Map;

import org.springblade.portrait.entity.TagsUser;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface TagsUserMapper extends BaseMapper<TagsUser> {

	/**
	 * 获取报表数据
	 * @param tagsId
	 * @param userId
	 * @return
	 */
	List<Map<String, Object>> getReportData(Integer tagsId, Long userId);
	
	/**
	 * 获取用户所有标签
	 * @param userId
	 * @return
	 */
	List<Map<String, Object>> getUserTags(Long userId);
}
