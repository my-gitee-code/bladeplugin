package org.springblade.portrait.service;

import org.springblade.portrait.entity.TagsData;

import com.baomidou.mybatisplus.extension.service.IService;

public interface ITagsDataService extends IService<TagsData> {

	/**
	 * 查询标签，不存在则增加
	 * @param typeId
	 * @param name
	 * @return
	 */
	TagsData findOrSave(Integer typeId, String name);
}
