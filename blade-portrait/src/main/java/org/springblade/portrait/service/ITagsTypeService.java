package org.springblade.portrait.service;

import org.springblade.portrait.entity.TagsType;

import com.baomidou.mybatisplus.extension.service.IService;

public interface ITagsTypeService extends IService<TagsType> {

}
