package org.springblade.portrait.service;

import java.util.List;
import java.util.Map;

import org.springblade.portrait.entity.TagsUser;

import com.baomidou.mybatisplus.extension.service.IService;

public interface ITagsUserService extends IService<TagsUser> {
	
	/**
	 * 新增用户标签
	 * @param userId
	 * @param destId
	 * @param tagId
	 * @param tagName
	 * @param item
	 * @return
	 */
	Boolean insert(Long userId, Long destId, Integer tagId, String tagName, Map<String, Object> item);
	
	/**
	 * 获取报表数据
	 * @param tagsId
	 * @param userIds
	 * @return
	 */
	Map getReportData(Integer tagsId, List<Long> userIds);

}
