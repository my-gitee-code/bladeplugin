package org.springblade.portrait.service.impl;

import org.springblade.portrait.entity.TagsData;
import org.springblade.portrait.mapper.TagsDataMapper;
import org.springblade.portrait.service.ITagsDataService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

@Service
public class TagsDataServiceImpl extends ServiceImpl<TagsDataMapper, TagsData> implements ITagsDataService {

	@Override
	public TagsData findOrSave(Integer typeId, String name) {
		LambdaQueryWrapper<TagsData> lqw = new LambdaQueryWrapper();
		lqw.eq(TagsData::getTypeId, typeId);
		lqw.eq(TagsData::getTagsName, name);
		
		if (count(lqw) > 0L) {
			return getOne(lqw);
		}
		
		TagsData data = new TagsData();
		data.setTypeId(typeId);
		data.setTagsName(name);
		save(data);
		
		return data;
	}
}
