package org.springblade.portrait.service.impl;

import org.springblade.portrait.entity.TagsType;
import org.springblade.portrait.mapper.TagsTypeMapper;
import org.springblade.portrait.service.ITagsTypeService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

@Service
public class TagsTypeServiceImpl extends ServiceImpl<TagsTypeMapper, TagsType> implements ITagsTypeService {

}
