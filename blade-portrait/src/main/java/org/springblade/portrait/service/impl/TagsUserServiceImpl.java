package org.springblade.portrait.service.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springblade.portrait.entity.TagsData;
import org.springblade.portrait.entity.TagsUser;
import org.springblade.portrait.mapper.TagsUserMapper;
import org.springblade.portrait.service.ITagsDataService;
import org.springblade.portrait.service.ITagsUserService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

@Service
public class TagsUserServiceImpl extends ServiceImpl<TagsUserMapper, TagsUser> implements ITagsUserService {

	@Resource
	private ITagsDataService tagsDataService;
	@Resource
	private TagsUserMapper tagsUserMapper;
	
	@Override
	public Boolean insert(Long userId, Long destId, Integer tagId, String tagName, Map<String, Object> item) {
		TagsData tagsData = tagsDataService.findOrSave(tagId, tagName);
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTime((Date)item.get("create_time"));
		
		TagsUser data = new TagsUser();
		data.setTagsId(tagsData.getId());
		data.setUserId(userId);
		data.setDestId(destId);
		data.setYear(calendar.get(Calendar.YEAR));
		data.setMonth(calendar.get(Calendar.MONTH));
		data.setWeek(Integer.parseInt(String.format("%d", item.get("week"))));
		return save(data);
	}

	@Override
	public Map getReportData(Integer tagsId, List<Long> userIds) {
		// TODO Auto-generated method stub

		Map result = new HashMap();
		List<Map<String, Object>> data = tagsUserMapper.getReportData(tagsId, null);
		result.put("0", data);
		
		if (userIds != null && userIds.size() > 0) {
			userIds.forEach(userId -> {
				List<Map<String, Object>> item = tagsUserMapper.getReportData(tagsId, userId);
				result.put(userId, item);
			});
		}
		
		return result;
	}

}
