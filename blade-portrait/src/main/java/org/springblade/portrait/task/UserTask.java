package org.springblade.portrait.task;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springblade.core.redis.cache.BladeRedis;
import org.springblade.portrait.entity.TagsType;
import org.springblade.portrait.service.ITagsTypeService;
import org.springblade.portrait.service.ITagsUserService;
import org.springblade.portrait.utils.ScriptUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class UserTask {

	@Resource
	private JdbcTemplate jdbcTemplate;
	@Resource
	private BladeRedis bladeRedis;
	
	@Resource
	private ITagsTypeService tagsTypeService;
	@Resource
	private ITagsUserService tagsUserService;
	
	@Value("${blade.portrait.sql}")
	private String mainSql = "select a.*,c.type_name,YEAR(a.create_time) as year,MONTH(a.create_time) as month,WEEKDAY(a.create_time) as week from tb_shop_reserve_finish as a,tb_shop_room as b,tb_shop_room_type as c where a.room_id=b.id and b.room_type=c.id and a.order_id>%d order by a.order_id asc";
	
	/**
	 * 数据分析:5每分钟运行
	 */
	@Scheduled(fixedDelay = 5 * 60000)
	synchronized public void dataAnalysis() {
		// select a.*,b.name,c.tags_name,b.expression,d.start_time from blade_tags_user as a,blade_tags_type as b,blade_tags_data as c,tb_shop_reserve_finish as d 
		// where a.tags_id=c.id and c.type_id=b.id and a.dest_id=d.order_id
		String idField = "order_id", userField = "user_id";
		String key = "blade:portrait:tags:mark:id";
		List<TagsType> tagsTypes = tagsTypeService.list();
		
		Long id = bladeRedis.get(key);
		if (id == null) {
			id = 0L;
		}
		
		String sql;
		if (mainSql.toLowerCase().indexOf(" limit ") == -1) {
			sql = String.format(mainSql, id).concat(" limit 100");
		} else {
			sql = String.format(mainSql, id);
		}
		log.info("SQL: {}", sql);
		List<Map<String, Object>> datas = jdbcTemplate.queryForList(sql);
		
		if (datas == null || datas.size() == 0) {
			return;
		}
		id = (Long)datas.get(datas.size() - 1).get(idField);
		bladeRedis.set(key, id);
		
		datas.forEach(item -> {
			tagsTypes.forEach(tags -> {
				if (tags.getExpression() != null && !tags.getExpression().isEmpty()) {
					Object result = item.containsKey(tags.getExpression()) ? item.get(tags.getExpression()) : ScriptUtils.invokeMethod(tags.getExpression(), item);
					if (result != null) {
						log.info("{},{} => {}", result, tags.getName(), item);
						
						if (result.getClass().getSimpleName().equals("ArrayList")) {
							((List<Object>)result).forEach(name -> {
								tagsUserService.insert((Long)item.get(userField), (Long)item.get(idField), tags.getId(), name.toString(), item);
							});
						} else {
							tagsUserService.insert((Long)item.get(userField), (Long)item.get(idField), tags.getId(), result.toString(), item);
						}
					}
				}
			});
		});
	}
}
