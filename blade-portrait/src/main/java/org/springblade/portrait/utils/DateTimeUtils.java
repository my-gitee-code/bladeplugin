package org.springblade.portrait.utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class DateTimeUtils {
	
	/**
	 * 获取小时
	 * @param date
	 * @return
	 */
	public static String getHour(Date date) {
//		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat sdf = new SimpleDateFormat("HH");
		return sdf.format(date);
	}
	
	/**
	 * 获取周几
	 * @param date
	 * @return
	 */
	public static String getWeek(Date date) {
		Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        
        // 获取指定日期所在周的星期几
        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        
        if (dayOfWeek == Calendar.SUNDAY) {
        	return "星期日";
        } else if (dayOfWeek == Calendar.MONDAY) {
        	return "星期一";
        } else if (dayOfWeek == Calendar.TUESDAY) {
        	return "星期二";
        } else if (dayOfWeek == Calendar.WEDNESDAY) {
        	return "星期三";
        } else if (dayOfWeek == Calendar.THURSDAY) {
        	return "星期四";
        } else if (dayOfWeek == Calendar.FRIDAY) {
        	return "星期五";
        } else if (dayOfWeek == Calendar.SATURDAY) {
        	return "星期六";
        }
        
        return "none";
	}
	
	/**
	 * 获取月份
	 * @param date
	 * @return
	 */
	public static String getMonth(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("MM");
		return sdf.format(date);
	}
	
	/**
	 * 两个时间相差多少个小时
	 * @param date1
	 * @param date2
	 * @return
	 */
	public static Integer getTimeDiffHour(Date date1, Date date2) {
		Long time = Math.abs(date1.getTime() - date2.getTime());
		String diff = String.format("%.0f", Math.ceil(time / (60 * 60 * 1000)));
		return Integer.parseInt(diff);
	}
	
	/**
	 * 两个时间之间，占用的小时
	 * @param date1
	 * @param date2
	 * @return
	 */
	public static List<String> getTimeOccupyHour(Date date1, Date date2) {
		List<String> hours = new ArrayList<>();
		SimpleDateFormat sdf = new SimpleDateFormat("HH");

		Integer hour = 0;
		Calendar calendar = Calendar.getInstance();
        calendar.setTime(date1);
        while (calendar.getTime().getTime() < date2.getTime()) {
        	hour++;
        	hours.add(sdf.format(calendar.getTime()));
        	calendar.add(Calendar.HOUR_OF_DAY, 1);
        }
        
		return hours;
	}
}
