package org.springblade.portrait.utils;

import java.util.Map;

import org.apache.commons.jexl2.Expression;
import org.apache.commons.jexl2.JexlContext;
import org.apache.commons.jexl2.JexlEngine;
import org.apache.commons.jexl2.MapContext;

public class ScriptUtils {
	
	/**
	 * 执行自定义表达式
	 * @param jexlExp
	 * @param map
	 * @return
	 */
    public static Object invokeMethod(String jexlExp, Map<String, Object> map) {
        JexlEngine jexl = new JexlEngine();
        Expression e = jexl.createExpression(jexlExp);
        JexlContext jc = new MapContext();
        jc.set("dateTimeUtils", DateTimeUtils.class);
        for(Map.Entry<String, Object> entry : map.entrySet()){
            jc.set(entry.getKey(), entry.getValue());
        }
        return e.evaluate(jc);
    }

}
