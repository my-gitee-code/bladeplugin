
CREATE TABLE `blade_os_process` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `name` varchar(100) COLLATE utf8mb4_bin NOT NULL COMMENT '名称',
    `directory` varchar(256) COLLATE utf8mb4_bin NOT NULL COMMENT '目录',
    `cmd` varchar(500) COLLATE utf8mb4_bin NOT NULL COMMENT '完整命令行',
    `description` varchar(256) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '描述',
    `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态:1=启用,2=禁用',
    `is_deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否已删除',
    `create_time` datetime NOT NULL COMMENT '新增或修改时间',
    `port` int(11) NOT NULL DEFAULT '0' COMMENT '端口',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='系统进程';

CREATE TABLE `blade_os_service` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `name` varchar(100) COLLATE utf8mb4_bin NOT NULL COMMENT '进程名称',
    `description` varchar(256) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '描述',
    `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态:1=启用,2=禁用',
    `is_deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否已删除',
    `create_time` datetime NOT NULL COMMENT '新增或修改时间',
    `port` int(11) NOT NULL DEFAULT '0' COMMENT '端口',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='系统服务';

CREATE TABLE `blade_pay_channel` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '支付名称',
  `title` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '支付渠道名称:微信小程序,微信公众号,汇旺财,支付宝',
  `appid` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '微信appid',
  `merchant_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '微信支付商户号,汇旺财门店编号',
  `private_key_path` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '私匙文件路径',
  `merchant_serial` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '秘匙',
  `app_key` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '微信APIv3密钥,汇旺财MD5密钥',
  `brokerage` double(10,5) DEFAULT '0.00000' COMMENT '支付手续费',
  `controller` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '接口(控制器)路径',
  `status` tinyint NOT NULL DEFAULT '0' COMMENT '状态:0=未审核,1=已启用,2=已停用',
  `create_time` datetime NOT NULL COMMENT '交易时间',
  `is_deleted` tinyint NOT NULL DEFAULT '0' COMMENT '删除标识',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='支付通道';


CREATE TABLE `blade_pay_finance` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `out_trade_no` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '订单号',
  `amount` int NOT NULL COMMENT '交易金额,单位:分',
  `brokerage` double(10,2) DEFAULT '0.00' COMMENT '交易手续费',
  `attach` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '附加信息',
  `transaction_type` tinyint NOT NULL DEFAULT '0' COMMENT '交易类型:1=支付;2=退款',
  `pay_mode` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '支付方式:微信公众号',
  `transaction_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '三方交易单号',
  `status` tinyint NOT NULL DEFAULT '0' COMMENT '状态:0=未处理;1=待处理;2=失败;3=成功;4=已转入退款',
  `params` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '扩展参数,json格式',
  `description` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备注',
  `create_time` datetime NOT NULL COMMENT '预处理时间',
  `channel_id` int NOT NULL COMMENT '支付通道编号',
  `error` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '失败原因',
  `op_user_id` bigint DEFAULT NULL COMMENT '操作员',
  `update_time` datetime DEFAULT NULL COMMENT '交易时间',
  `out_refund_no` varchar(100) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '退款单号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='交易记录';


CREATE TABLE `blade_pay_prorate` (
  `id` int NOT NULL AUTO_INCREMENT,
  `channel_id` int NOT NULL COMMENT '支付通道编号',
  `type` varchar(20) COLLATE utf8mb4_bin NOT NULL COMMENT '分账接收方类型:MERCHANT_ID,PERSONAL_OPENID',
  `account` varchar(50) COLLATE utf8mb4_bin NOT NULL COMMENT '分账接收方账号:MERCHANT_ID=商户号,PERSONAL_OPENID=个人openid',
  `name` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '分账个人接收方姓名',
  `relation_type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '与分账方的关系类型:STORE：门店;STAFF：员工;STORE_OWNER：店主;PARTNER：合作伙伴;HEADQUARTER：总部;BRAND：品牌方;DISTRIBUTOR：分销商;USER：用户;SUPPLIER： 供应商;CUSTOM：自定义',
  `custom_relation` varchar(20) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '自定义的分账关系:如代理商',
  `brokerage` double(10,5) NOT NULL DEFAULT '0.00000' COMMENT '分账比例',
  `status` tinyint NOT NULL DEFAULT '0' COMMENT '状态:0=禁用;1=启用',
  `description` varchar(100) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备注',
  `create_time` datetime NOT NULL,
  `is_deleted` tinyint NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='分帐设置';


CREATE TABLE `tb_report_dataset` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '名称',
  `source_id` int NOT NULL COMMENT '所属数据源',
  `table_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '表名',
  `description` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '描述',
  `deploy` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin COMMENT '配置，json数据',
  `param` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '查询参数',
  `query` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '查询SQL',
  `status` tinyint NOT NULL DEFAULT '0' COMMENT '状态：1=启用',
  `create_time` datetime NOT NULL COMMENT '新增或修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='数据集';

CREATE TABLE `tb_report_datasource` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '名称',
  `driver` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '数据库驱动',
  `url` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '连接字符串',
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户',
  `password` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '密码',
  `status` tinyint NOT NULL DEFAULT '0' COMMENT '状态：1=启用',
  `create_time` datetime NOT NULL COMMENT '新增或修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='数据源';

CREATE TABLE `tb_report_model` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL COMMENT '名称',
  `dataset_id` int NOT NULL COMMENT '数据集编号',
  `data_fields` varchar(500) NOT NULL COMMENT '数据字段',
  `census_field` varchar(100) NOT NULL COMMENT '统计字段',
  `unit` varchar(20) DEFAULT NULL COMMENT '单位',
  `description` varchar(256) DEFAULT NULL COMMENT '说明',
  `status` tinyint NOT NULL DEFAULT '0' COMMENT '状态：1=启用',
  `create_time` datetime NOT NULL COMMENT '新增或修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='报表模型';
